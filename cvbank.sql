-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2017 at 02:26 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cvbank`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE IF NOT EXISTS `abouts` (
`id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `work_area` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `bio` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `unique_id`, `user_id`, `phone`, `work_area`, `short_desc`, `bio`, `created_at`, `updated_at`, `deleted_at`) VALUES
(54, '', 39, '01747806948', 'Designer,Developer', 'Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book', '   Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions ', '0000-00-00 00:00:00', '2017-02-10 07:00:09', '0000-00-00 00:00:00'),
(55, '', 40, '01753736374', 'Developer,Designer,Artist', 'Good I am fine And alwas like to feel good.', ' It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldu', '2017-02-10 07:48:09', '2017-02-11 03:59:20', '0000-00-00 00:00:00'),
(56, '', 41, '01879830024', 'Web developer', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. ', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. ', '2017-02-10 07:54:27', '2017-02-11 04:30:29', '0000-00-00 00:00:00'),
(57, '', 42, '01734496525', 'developer', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. ', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsu', '2017-02-11 04:44:20', '2017-02-11 04:47:53', '0000-00-00 00:00:00'),
(58, '', 43, '01936066425', 'HTML , PHP', 'In WCAG2ICT the question came up as to whether the name of a document or a web application was sufficient to represent its Purpose or Topic. The question arose because of the language &#34; that describe topic or purpose&#34;. This shows up in 2.4.4 (link', ' â€œLife on earth is a whole, yet it expresses itself in unique time-bound bodies, microscopic or visible, plant or animal, extinct or living. So there can be no one place to be. There can be no one way to be, no one way to practice, no one way to learn, no one way to love, no one way to grow or to heal, no one way to live, no one way to feel, no one thing to know or be known. The particulars count.â€', '2017-02-11 05:22:32', '2017-02-11 05:26:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE IF NOT EXISTS `awards` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `organization` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `year` year(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `awards`
--

INSERT INTO `awards` (`id`, `user_id`, `unique_id`, `title`, `organization`, `description`, `location`, `year`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 39, '589dc8a203b6e', 'web developer', 'Bitm', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic type', 'Dhaka,Bangladesh', 2017, '2017-02-10 03:05:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 39, '589dc8badd821', 'web developer', 'webtech', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic type', 'Dhaka,Bangladesh', 2016, '2017-02-10 03:05:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 40, '589e7f903d489', 'New Programer', 'Webtech', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat.. ', 'London', 2013, '2017-02-11 04:05:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 41, '589e8638e7e71', 'web developer', 'webtech', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 'Dhaka,Bangladesh', 2016, '2017-02-11 04:34:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 42, '589e8cb8d6783', 'Journalist', 'The Daily Peoples Time', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. ', 'Dhaka,Bangladesh', 2016, '2017-02-11 05:02:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `user_id`, `unique_id`, `name`, `email`, `message`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(21, 39, '589dcdc68548b', 'Pallab', 'pallabcse14@gmail.com', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 0, '2017-02-10 03:27:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 39, '589dd3a501d9d', 'Pallab', 'pallabcse14@gmail.com', 'h c;unrt;vune,v; by', 0, '2017-02-10 03:52:21', '0000-00-00 00:00:00', '2017-02-10 03:54:35'),
(23, 39, '589e0245aa7f8', 'MD NASIR FARDOUSH', 'nasirfardoush.bd@gmail.com', 'Hello Pallab How are you', 0, '2017-02-10 07:11:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 39, '589e0468049a9', 'Ryhan Kobil', 'ryhan@gmail.com', 'ook a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, ', 0, '2017-02-10 07:20:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 40, '589e83be64414', 'Pallab', 'pallabcse14@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ', 0, '2017-02-11 04:23:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 41, '589e87b9f3e41', 'Pallab', 'pallabcse14@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 0, '2017-02-11 04:40:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 41, '589ea7cb65134', 'MD NAsir Fardoush', 'engg.about@gmail.com', 'Test Your contact o01753736374', 0, '2017-02-11 06:57:31', '0000-00-00 00:00:00', '2017-02-11 06:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

CREATE TABLE IF NOT EXISTS `educations` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `degree` varchar(255) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `result` varchar(255) NOT NULL,
  `enrolled_year` year(4) NOT NULL,
  `passing_year` year(4) NOT NULL,
  `education_board` varchar(255) NOT NULL,
  `course_duration` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `location` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`id`, `user_id`, `unique_id`, `title`, `degree`, `institute`, `result`, `enrolled_year`, `passing_year`, `education_board`, `course_duration`, `created_at`, `updated_at`, `deleted_at`, `location`) VALUES
(18, 39, '589dc77ded7d4', 'SSC', 'SSC', 'Lalmonirhat Govt high school', '5', 2008, 2010, 'Dinajpur', '2', '2017-02-10 03:00:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Lalmonirhat, Bangladesh'),
(19, 39, '589dc7c220940', 'HSC', 'HSC', 'Lalmonirhat Govt College', '5', 2010, 2012, 'Dinajpur', '2', '2017-02-10 03:01:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Lalmonirhat, Bangladesh'),
(20, 39, '589dca33a0e93', 'Computer science', 'Bsc', 'Daffodil international University', '3.50', 2015, 2017, 'n/a', '4', '2017-02-10 03:12:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Dhaka,Bangladesh'),
(21, 40, '589e7e91cfad8', 'Computer science and engineering', 'Bsc', 'Asian University Of Bangladesh', '3.83', 2015, 2017, '', '4', '2017-02-11 04:01:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Bangladesh'),
(22, 40, '589e7f1ad9231', 'Electronics Engineering', 'Diploma', 'Barisal Polytechnic', '3.83', 2010, 2013, 'Technical', '4', '2017-02-11 04:03:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Bangladesh'),
(23, 41, '589e85f29b7d2', 'Computer science and Engineering', 'B.Sc', 'Daffodil international University', '3.45', 2013, 2017, '', '', '2017-02-11 04:33:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Dhaka,Bangladesh'),
(24, 42, '589e8bb250eaf', 'Public Administration', 'MSS', 'University of Dhaka', '3.50', 2015, 2016, '', '1', '2017-02-11 04:57:38', '2017-02-11 04:59:38', '0000-00-00 00:00:00', 'Dhaka,Bangladesh'),
(25, 42, '589e8c022b3cc', 'Public Administration', 'BSS', 'University of Dhaka', '3.27', 2009, 2013, '', '4', '2017-02-11 04:58:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Dhaka,Bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE IF NOT EXISTS `experiences` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `expreince_desc` varchar(255) NOT NULL,
  `start_date` year(4) NOT NULL,
  `end_date` year(4) NOT NULL,
  `company_location` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experiences`
--

INSERT INTO `experiences` (`id`, `user_id`, `unique_id`, `designation`, `company_name`, `expreince_desc`, `start_date`, `end_date`, `company_location`, `created_at`, `updated_at`, `deleted_at`) VALUES
(18, 39, '589dc869dd8de', 'Web developer and designer', 'Bitm', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic type', 2014, 2017, 'Dhaka bangladesh', '2017-02-10 03:04:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 39, '589dc884c5732', 'Web developer and designer', 'webtech', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic type', 2010, 2014, 'Dhaka bangladesh', '2017-02-10 03:04:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 40, '589e7f5550f27', 'Junior web developer', 'BDtech', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat.', 2014, 2016, 'Dhaka', '2017-02-11 04:04:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 41, '589e86228fbc3', 'We', 'Bitm', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 2016, 2017, 'Dhaka bangladesh', '2017-02-11 04:33:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 42, '589e8c6705d73', 'Journalist', 'The daily peoples time', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus.', 2015, 2017, 'Dhaka bangladesh', '2017-02-11 05:00:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `facts`
--

CREATE TABLE IF NOT EXISTS `facts` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `no_of_items` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facts`
--

INSERT INTO `facts` (`id`, `user_id`, `unique_id`, `title`, `no_of_items`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(17, 39, '589dc68ad8ff2', 'web developer', 50, '5622680.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 39, '589dc6a987078', 'php Developer', 52, 'd8dbc0f.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 40, '589e7e2cd6284', 'Web design', 24, '9ff567b.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 40, '589e7e464ddca', 'Web developed', 10, '9399ec8.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 41, '589e85a147088', 'web developer', 50, '3725c9c.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 42, '589e8acb10383', 'web developer', 50, 'c287fa5.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 42, '589e8b3963087', 'Journalist', 50, 'ccb5a33.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 43, '589e932ec681c', 'web developer', 25, 'a46dab1.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `user_id`, `unique_id`, `title`, `description`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(41, 39, '589dc651ed361', 'Fishing', 'My hobby is riding a bicycle.I choose this hobby because,it can make the body always health.Riding a bicycle is does not a hard activity and we can do it in anywhere.According to me riding a bicycle is a good hobby because,it does not fool away many time ', '8cbf89c.jpg', '2017-02-10 02:55:29', '2017-02-10 07:30:34', '0000-00-00 00:00:00'),
(42, 39, '589dc662d212c', 'Cycling', 'My hobby is riding a bicycle.I choose this hobby because,it can make the body always health.Riding a bicycle is does not a hard activity and we can do it in anywhere.According to me riding a bicycle is a good hobby because,it does not fool away many time ', 'dcc2561.jpg', '2017-02-10 02:55:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 40, '589e7de1e63a4', 'Coding', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat. Praesent sit amet pretium libero. Nullam non blandit orci. Aliquam porttitor leo id v', '98ed536.jpg', '2017-02-11 03:58:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 41, '589e8587e64aa', 'Fishing', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 'fca143b.jpg', '2017-02-11 04:31:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 42, '589e89a0815cb', 'Cycling', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada', '0b436d0.jpg', '2017-02-11 04:48:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 42, '589e8a4f3eca2', 'Travelling', 'I am extensive traveler. I would like to visit newer places and see the unseen.', 'ae5e075.jpg', '2017-02-11 04:51:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 42, '589e8aad4b305', 'Reading Books', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. ', '0733763.jpg', '2017-02-11 04:53:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 43, '589e9305ace7c', 'web developer', 'for hobbies away from the puter: fishing, mountaineering, boating, music (listening, composing and playing), reading, photographing, video editing, waterskiing, skiing, snowboarding, snowmobiles & ATV&#39;s, hypnosis (been playing with it for 18yrs), writ', '1cc2cf9.jpg', '2017-02-11 05:28:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE IF NOT EXISTS `portfolios` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `user_id`, `unique_id`, `title`, `description`, `img`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(13, 39, '589dc9c49f380', 'web developer', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'db88aef.jpg', 'Php', '2017-02-10 03:10:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 39, '589dcc350a89c', 'Laraval', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '3c68f60.png', 'Php', '2017-02-10 03:20:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 40, '589e834174375', 'Web developed', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. ', 'a0ecc4f.jpg', 'Web developing', '2017-02-11 04:21:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 41, '589e872714a83', 'web developer', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', '4fb6183.jpg', 'Php', '2017-02-11 04:38:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 42, '589e8e2dab666', 'Journalist', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. ', '76258c6.jpg', 'Journalist', '2017-02-11 05:08:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 40, '589e974e7079c', 'Web developed', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and', '6fdeece.jpg', 'Web developing', '2017-02-11 05:47:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 40, '589e976ff22ba', 'Design service', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and', 'c1ed397.jpg', 'Design', '2017-02-11 05:47:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 40, '589e9793378c8', 'Develoved', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and', 'b4768aa.png', 'Design', '2017-02-11 05:48:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tags` varchar(255) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `unique_id`, `title`, `img`, `author_name`, `city_name`, `country_name`, `description`, `tags`, `categories`, `created_at`, `updated_at`, `deleted_at`) VALUES
(12, 39, '589dc806cc10a', 'Laraval', '777e8c2.jpg', 'Pallab', 'Dhaka', 'Bangladesh   ', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'php frame work', 'coding', '2017-02-10 03:02:46', '2017-02-11 06:38:15', '0000-00-00 00:00:00'),
(13, 39, '589dc8f355576', 'Learn php', '6105875.jpg', 'Pallab', 'Dhaka', 'Bangladesh ', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'php', 'Education', '2017-02-10 03:06:43', '2017-02-10 06:47:03', '0000-00-00 00:00:00'),
(14, 40, '589e801882ab9', 'How to game', 'e7dc4cb.jpg', 'Nasir', 'Bangladesh', 'Bangladesh ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat. Praesent sit amet pretium libero.', 'php', 'Game', '2017-02-11 04:08:08', '2017-02-11 06:26:05', '0000-00-00 00:00:00'),
(15, 41, '589e8669a1080', 'Learn php', '2ab33bf.jpg', 'Liza', 'Dhaka', 'Bangladesh  ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat\r\nelit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat \r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat ', 'php', 'Education', '2017-02-11 04:35:05', '2017-02-11 05:59:09', '0000-00-00 00:00:00'),
(16, 42, '589e8d5e83912', 'PHP', '9048432.jpg', 'Rokan', 'Dhaka', 'Bangladesh', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et .', 'php', 'Education', '2017-02-11 05:04:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 40, '589e97110b132', 'Web developed', '9e4950d.jpg', 'Nasir', 'Dhaka', 'Bangladesh', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and', 'php', 'Education', '2017-02-11 05:46:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 40, '589ea0e20140d', 'Web developed', '6e6a54a.jpg', 'Nasir', 'Dhaka', 'Bangladesh', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unced not only five centuries, but also the leap into electronic typesetting, remaining essentially unc', 'php', 'Work', '2017-02-11 06:28:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `topics` longtext NOT NULL,
  `img` varchar(255) NOT NULL,
  `client_image` varchar(255) NOT NULL,
  `clinte_feedback` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `user_id`, `unique_id`, `title`, `description`, `topics`, `img`, `client_image`, `clinte_feedback`, `created_at`, `updated_at`, `deleted_at`) VALUES
(30, 39, '589dca9e28fea', 'web developer', '											 Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.										', 'Responsive Design,PHPD to HTML, JAVASCRIPT INTEGRETED', 'a7467d4.jpg', 'clienta7467.jpg', '											He is good in php										', '2017-02-10 03:13:50', '2017-02-10 07:02:22', '0000-00-00 00:00:00'),
(31, 39, '589dcb9316a62', 'SEO SERVICES', '											 Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.										', 'Google analytics', '307c468.jpg', 'client307c4.jpg', '											I am a good designer										', '2017-02-10 03:17:55', '2017-02-10 07:03:26', '0000-00-00 00:00:00'),
(32, 40, '589e80ef850c5', 'Design service', 'adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat. Praesent sit amet pretium libero.																														', '																																	Webdesign																														', '0221844.jpg', 'client02218.jpg', '																																	He is a good Programmer.																														', '2017-02-11 04:11:43', '2017-02-11 04:13:08', '2017-02-11 04:13:40'),
(33, 40, '589e81be5b1ed', 'Web Developing', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was', '14cac3c.jpg', 'client14cac.jpg', '																						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. 																				', '2017-02-11 04:15:10', '2017-02-11 06:29:37', '0000-00-00 00:00:00'),
(34, 41, '589e86b94b3b6', 'Web designer', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 'Web developer', '14f73bd.jpg', 'client14f73.jpg', 'I am a good programmer.', '2017-02-11 04:36:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 42, '589e8f04f2fcd', 'Journalist', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 'Journalism', 'd12a6b3.jpg', 'clientd12a6.jpg', 'I am an efficient and goal oriented person.', '2017-02-11 05:11:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `themecolor` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `featured_img` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `user_id`, `title`, `fullname`, `themecolor`, `address`, `featured_img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(12, 39, 'web developer', 'Pallab', 'blue', 'Dhaka,Bangladesh', 'efb8286.jpg', '0000-00-00 00:00:00', '2017-02-10 03:38:39', '0000-00-00 00:00:00'),
(13, 40, 'Full Stack Web Developer', 'MD NASIR FARDOUSH', 'green', 'Gazipure-Bangladesh', '1b614e3.png', '2017-02-10 07:48:09', '2017-02-11 05:51:39', '0000-00-00 00:00:00'),
(14, 41, 'PHP Developer', 'Jannatul Ferdoush', 'blue', 'Dhaka,Bangladesh', '5566e0a.jpg', '2017-02-10 07:54:27', '2017-02-11 07:49:02', '0000-00-00 00:00:00'),
(15, 42, 'web developer', 'Rokanuzzaman', 'blue', 'Dhaka,Bangladesh', '6c905d1.jpg', '2017-02-11 04:44:20', '2017-02-11 04:46:57', '0000-00-00 00:00:00'),
(16, 43, 'web developer', 'shemul dey', 'cyan', 'Dhanmondi, Dhaka', '1b7b935.jpg', '2017-02-11 05:22:32', '2017-02-11 05:38:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `level` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `experience_area` varchar(255) NOT NULL,
  `category` tinyint(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `user_id`, `unique_id`, `title`, `description`, `level`, `experience`, `experience_area`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(14, 39, '589dc96cb63e6', 'Programming', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Intermediate', '4', 'Php, C,C+,Java', 2, '2017-02-10 03:08:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 40, '589e830a9d091', 'Programming', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula.', 'Advance', '2', 'Php,html', 4, '2017-02-11 04:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 41, '589e877ec648e', 'Programming', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 'Experts', '3', 'PHP ,HTML,CSS', 4, '2017-02-11 04:39:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 42, '589e8dec1e141', 'Journalist', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. t', 'Intermediate', '2', 'translating, proof reading', 2, '2017-02-11 05:07:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 40, '589e98a08f69d', 'Web Development', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was', 'Advance', '4', 'PHP,AJAX,JAVA', 1, '2017-02-11 05:52:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `teaching`
--

CREATE TABLE IF NOT EXISTS `teaching` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `teaching_desc` text NOT NULL,
  `start_date` year(4) NOT NULL,
  `end_date` year(4) NOT NULL,
  `teaching_status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teaching`
--

INSERT INTO `teaching` (`id`, `user_id`, `unique_id`, `title`, `institute`, `teaching_desc`, `start_date`, `end_date`, `teaching_status`, `created_at`, `deleted_at`, `updated_at`) VALUES
(11, 39, '589dc936cf0e2', 'Teacher', 'Daffodil international University', ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 2015, 2016, 'PREVIOUS', '2017-02-10 03:07:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 40, '589e82b6af559', 'Asistant Professor', 'Asian University Of Bangladesh', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula.', 2015, 2016, 'PREVIOUS', '2017-02-11 04:19:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 41, '589e86fa3ac70', 'Proffesor', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 2016, 2017, 'PREVIOUS', '2017-02-11 04:37:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 42, '589e8d8dbdb44', 'Proffesor', 'Dhaka University', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at sapien ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat', 2015, 2017, 'CURRENT', '2017-02-11 05:05:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 40, '589e97e8b9757', 'Asistant Professor', 'DIU', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was', 2012, 2017, 'PREVIOUS', '2017-02-11 05:49:44', '0000-00-00 00:00:00', '2017-02-11 05:50:28'),
(16, 40, '589e98049bac5', 'Instructor', 'MCET', 'ed not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was', 2016, 2017, 'CURRENT', '2017-02-11 05:50:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `user_role` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `username`, `first_name`, `last_name`, `email`, `password`, `token`, `is_active`, `user_role`, `created_at`, `updated_at`, `deleted_at`) VALUES
(39, '¼Ü[)§Ñîá‰œãê²–L9º=', 'Pallab', 'hk', 'Roy', 'pallabcse14@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', 'Test', 0, 2, '2017-02-10 02:49:08', '2017-02-11 07:03:02', '0000-00-00 00:00:00'),
(40, 'ÈÔ˜@D5PÍmß¸íï˜Xî\ZL', 'nasir', 'MD NASIR', 'FARDOUSH', 'nasirfardoush.bd@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', 'Test', 0, 3, '2017-02-10 07:48:09', '2017-02-10 08:03:12', '0000-00-00 00:00:00'),
(41, '{—¹:¥95ü²­Ö‚))ûéª', 'Liza', 'Jannatul', 'Ferdoush', 'liza@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', 'Test', 0, 2, '2017-02-10 07:54:27', '2017-02-11 07:03:21', '0000-00-00 00:00:00'),
(42, 'I	j5ä(îpú³•‡l*ÿÃŠ‚{e', 'Rokan ', 'Md. ', 'Rokanuzzaman', 'rokandu@yahoo.com', 'fcea920f7412b5da7be0cf42b8c93759', 'Test', 0, 2, '2017-02-11 04:44:20', '2017-02-11 07:03:26', '0000-00-00 00:00:00'),
(43, '{Ç§ðñÉ›ùÀ¬5ž©²ã¶', 'shemuldey', 'shemul', 'dey', 'shemul807@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', 'Test', 0, 2, '2017-02-11 05:22:32', '2017-02-11 07:03:35', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `educations`
--
ALTER TABLE `educations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facts`
--
ALTER TABLE `facts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teaching`
--
ALTER TABLE `teaching`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `educations`
--
ALTER TABLE `educations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `experiences`
--
ALTER TABLE `experiences`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `facts`
--
ALTER TABLE `facts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `teaching`
--
ALTER TABLE `teaching`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
