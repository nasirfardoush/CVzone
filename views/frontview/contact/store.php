<?php 
include_once ('../../../vendor/autoload.php');
use App\frontview\Contact;
$objcontact = new Contact;


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if ( !empty($_POST['name']) AND !empty($_POST['message'])  AND !empty($_POST['email'])){

			if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
						$_SESSION['email'] = "Please Provide a valid email address !!";
						header('Location:../index.php');
					}else{
						$objcontact->setData($_POST)->store();
				}
		}else{
			$_SESSION['fail'] = "Fields are required!";
			header('Location:../index.php?section=8');
		}	
	}
