<?php
include("../../vendor/mpdf/mpdf/mpdf.php");
include_once ('../../vendor/autoload.php');
use App\frontview\Frontview;
$obj = new Frontview($_GET);
$allData = $obj->index();



/*User identification*/
$email   = $allData['email'];
foreach ($allData['settings'] as $settingsData) { };
	$name 	 = $settingsData['fullname'];
	$address = $settingsData['address'];
	$image = $settingsData['featured_img'];
	$image = (!empty($image))?$image:'man.jpg';
foreach ($allData['abouts'] as $aboutsData) { };
	$phone 	 = $aboutsData['phone'];
	$myself  = $aboutsData['short_desc'];

$eduInfo  = "";
foreach ($allData['educations'] as $eduData) {

		$eduInfo .= "<tr>";
		$eduInfo .= "<td>".$eduData['title']."</td>";
		$eduInfo .= "<td>".$eduData['degree']."</td>";
		$eduInfo .= "<td>".$eduData['institute']."</td>";   
		$eduInfo .= "<td>".$eduData['course_duration']; echo (!empty($eduData['course_duration']))?'Years':'---'."</td>";
		$eduInfo .= "<td>".$eduData['enrolled_year']."-".$eduData['passing_year']."</td>";
        $eduInfo .= "<td>".$eduData['result']."</td>";     
		$eduInfo .= "<td>".$eduData['passing_year']."</td>";
		$eduInfo .= "<td>".$eduData['education_board']."</td>";
}

 $expreincesInfo  = ''; 
foreach ($allData['experiences'] as $expreincesData) {
    $expreincesInfo .= "<tr>"; 
    $expreincesInfo .= "<td colspan='2'>".$expreincesData['designation']."</td>"; 
    $expreincesInfo .= "<td colspan='3'>".$expreincesData['company_name']."</td>"; 
    $expreincesInfo .= "<td colspan='2'>".$expreincesData['company_location']."</td>"; 
    $expreincesInfo .= "<td>".$expreincesData['start_date']."-".$expreincesData['end_date']."</td>";
    $expreincesInfo .= "</tr>";  

}

$awardsInfo  = '';
foreach ($allData['awards'] as $awardsData) {
    $awardsInfo .= "<tr>";
    $awardsInfo .= "<td colspan='2'>".$awardsData['title']."</td>";
    $awardsInfo .= "<td colspan='3'>".$awardsData['organization']."</td>";
    $awardsInfo .= "<td colspan='2'>".$awardsData['location']."</td>";
    $awardsInfo .= "<td>".$awardsData['year']."</td>";
    $awardsInfo .= "</tr>";
}

 $servicesInfo  = '';
foreach ($allData['services'] as $servicesData) {
    $servicesInfo .= "<tr>";
    $servicesInfo .= "<td colspan='2' >".$servicesData['title']."</td>";
    $servicesInfo .= "<td colspan='2'>".$servicesData['topics']."</td>";
    $servicesInfo .= "<td colspan='4'>".$servicesData['clinte_feedback']."</td>";
    $servicesInfo .= "</tr>";
}


$html=<<<EOD

<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: Arial;
            font-size: 11px;
            color: #000;
        }
        body {
            width: 100%;
            font-family: Arial;
            font-size: 10pt;
            margin: 0;
            padding: 0;
        }
        p {
            margin: 0;
            padding: 0;
        }
        #wrapper {
            width: 180mm;
            margin: 0 15mm;
        }
        .page {
            height: 297mm;
            width: 210mm;
            page-break-after: always;
        }
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }
        table td {
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 2mm;
        }
        table {
            margin: 0px auto;
            background: #fcfcfc;
            padding: 20px;
            max-width: 900px;
            font-family: 'Open Sans', sans-serif;
        }        

    </style>

</head>

<body>

    <div class="container">

        <!-- Image and address -->
        <div class="row cvWraper">
            <div class="">
                <table border="1" cellspacing="0">
                    <tr>
                        <td colspan="6">
                            <h3 class="cvTitle">CV  <span>of </span></h3>
                            <h4 class="cvTitle">$name</h4>
                            <h5>$address</h5>
                            <h5>$phone</h5>
                            <h5>$email</h5>
                        </td>
                        <td colspan="2">
                            <img width="150" height="130" src="../../assets/images/$image">
                        </td>
                    </tr>
                    <!--Start Education Part -->
                    <tr>
                        <td colspan="8"><strong>Myself with few words:</strong>
                            <p class="text-justify">  $myself </p>
                        </td>
                    </tr>
                    <tr> <td colspan="8"><h5>Educations</h5></td></tr>
                    <tr>
                        <td>Title</td>
                        <td>Degree</td>
                        <td>Institute</td>
                        <td>Duration</td>
                        <td>Duration Years</td>
                        <td>Result</td>
                        <td>Passing Year</td>
                        <td>Board</td>
                    </tr>
                    $eduInfo;

                    <!--Start Education Part -->

                    <!-- Start	Experience Part -->
                    <tr>
                        <td colspan="8">
                            <h5>Experiences</h5>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">TItle</td>
                        <td colspan="3">Company/Organization </td>
                        <td colspan="2">Location</td>
                        <td>Duration Year</td>
                    </tr>
                    $expreincesInfo;
                   
                    <!-- End Experience Part -->

                    <!-- Start	Awards Part -->
                    <tr>
                        <td colspan="8">
                            <h5>Awards</h5>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">TItle</td>
                        <td colspan="3">Organization/Institute</td>
                        <td colspan="2">Location</td>
                        <td>Achieved Year</td>
                    </tr>
                    $awardsInfo;
                    <!-- End Awards Part -->


                    <!-- Start	Services Part -->
                    <tr>
                        <td colspan="8">
                            <h5>Services</h5>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Title</td>
                        <td colspan="2">Topics</td>
                        <td colspan="4">Client Feedback</td>
                    </tr>
                    $servicesInfo;

                    <!-- End Services Part -->


                </table>





            </div>
        </div>

    </div>
</body>
</html
EOD;
$mpdf = new mPDF('s', array(210,297));
$mpdf->mirrorMargins = 1;
$mpdf->bleedMargin = 4;

 //Set left to right text
$mpdf->SetDirectionality('ltr'); 
$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
 
$mpdf->WriteHTML($html);
         
$mpdf->Output();

exit;
?>
