<!-- Include header -->
<?php include_once('inc/header.php'); ?>
 <!-- hs-content-wrapper -->
      <div class="hs-content-wrapper">
      
         <!-- About section -->
             <?php include_once('inc/about.php'); ?>
             <!-- End About Section -->

                <!-- Resume Section -->
                <?php include_once('inc/resume.php'); ?>
                <!-- End Resume Section-->

                <!-- Publication Section -->
                    <?php include_once('inc/publication.php'); ?>
                <!-- End Publication Section -->

                <!-- service Section -->
                <?php include_once('inc/service.php'); ?>
                <!-- End service Section -->

                <!-- Teaching Section -->
                <?php include_once('inc/teaching.php'); ?>
                <!-- End Teaching Section -->

                <!-- Skills Section -->
                <?php include_once('inc/skills.php'); ?>
                <!-- End Skills Section -->

                <!-- Works Section -->
                <?php include_once('inc/works.php'); ?>
                <!-- End Works Section -->

                <!-- Contact Section -->
                <?php include_once('inc/contact.php'); ?>
                <!-- End Contact Section -->

                </div>
                <!-- End hs-content-wrapper -->
            </div>
            <!-- End hs-content-scroller -->
        </div>
        <!-- End container -->
<!--  Include footer -->       
<?php include_once('inc/footer.php'); ?>       