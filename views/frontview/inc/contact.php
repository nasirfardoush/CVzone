


<article class="hs-content contact-section" id="section8">
    <span class="sec-icon fa fa-paper-plane"></span>
    <div class="hs-inner">
        <span class="before-title">.08</span>
        <h2>CONTACT</h2>
        <div class="contact_info">
            <h3>Get in touch</h3>
            <hr>
            <h5>We are waiting to assist you</h5>
            <h6>Simply use the form below to get in touch</h6>
            <?php if (!isset($_SESSION)) {
               session_start();
            }
            if (isset($_SESSION['msgSend'])) {
               echo "<h5 class='text-success '>".$_SESSION['msgSend']."</h5>";
               unset($_SESSION['msgSend']);
            } ?>
            <hr>
        </div>
        <!-- Contact Form -->
       <form action="contact/store.php" method="POST">
            <fieldset id="contact_form">
                <div id="result"></div>
                <input type="text" name="name" id="name" placeholder="NAME" />

                <input type="email" name="email" id="email" placeholder="EMAIL" />

                <textarea name="message" id="message" placeholder="MESSAGE"></textarea>
                <?php foreach ($allData['settings'] as $data) { } ?>

                <input type="hidden" value="<?php echo $data['user_id']?>" name="id">

                <input  class="submit_btn" type="submit" value="SEND MESSAGE" name="contact">
            </fieldset>
      </form>  
        <!-- End Contact Form -->
    </div>
</article>