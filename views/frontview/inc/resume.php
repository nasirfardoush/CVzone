<article class="hs-content resume-section" id="section2">
    <span class="sec-icon fa fa-newspaper-o"></span>
    <div class="hs-inner">
        <span class="before-title">.02</span>
        <h2>RESUME</h2>
        <!-- Resume Wrapper -->
        <div class="resume-wrapper">
            <ul class="resume">
                <!-- Resume timeline -->
                <li class="time-label">
                    <span class="content-title">EDUCATION</span>
                </li>
                <?php foreach ($allData['educations'] as $eduData) { ?>
                      
                <li>
                    <div class="resume-tag">
                        <span class="fa fa-graduation-cap"></span>
                        <div class="resume-date">
                            <span><?php echo $eduData['enrolled_year']; ?></span>
                            <div class="separator"></div>
                            <span><?php echo $eduData['passing_year']; ?></span>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <span class="timeline-location"><i class="fa fa-map-marker"></i><?php echo $eduData['location']; ?></span>
                        <h3 class="timeline-header"><?php echo $eduData['title']; ?>- <?php echo $eduData['degree']; ?></h3>
                        <div class="timeline-body">
                            <h4><?php echo $eduData['institute']; ?></h4>
                            <span>
                                <p><?php // $eduData['description']; ?> </p>
                                <label>Result:</label><?php echo $eduData['result']; ?>
                                <label> Board:</label><?php echo $eduData['education_board']; ?>
                                <label> Durations:</label><?php echo $eduData['course_duration']; ?>
                            </span>
                        </div>
                    </div>
                </li>
                <?php } ?>    
                <li class="time-label">
                    <span class="content-title">ACADEMIC AND PROFESSIONAL POSITIONS</span>
                </li>

                <?php foreach ($allData['experiences'] as $experData) { ?>
                <li>
                    <div class="resume-tag">
                        <span class="fa fa-university"></span>
                        <div class="resume-date">
                            <span><?php echo $experData['start_date']; ?></span>
                            <div class="separator"></div>
                            <span><?php echo $experData['end_date']; ?></span>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <span class="timeline-location"><i class="fa fa-map-marker"></i><?php echo $experData['company_location']; ?></span>
                        <h3 class="timeline-header"><?php echo $experData['designation']; ?></h3>
                        <div class="timeline-body">
                            <h4><?php echo $experData['company_name']; ?></h4>
                            <span><?php echo $experData['expreince_desc']; ?></span>
                        </div>
                    </div>
                 </li>   
                <?php } ?> 
                <li class="time-label">
                    <span class="content-title">HONORS AND AWARDS</span>
                </li>
                <?php foreach ($allData['awards'] as $awardsData) { ?>
                <li>
                    <div class="resume-tag">
                        <span class="fa fa-star-o"></span>
                        <div class="resume-date">
                            <span><?php echo $awardsData['year']; ?></span>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <span class="timeline-location"><i class="fa fa-map-marker"></i><?php echo $awardsData['location']; ?></span>
                        <h3 class="timeline-header"><?php echo $awardsData['title']; ?></h3>
                        <div class="timeline-body">
                            <h4><?php echo $awardsData['organization']; ?></h4>
                            <span><?php echo $awardsData['description']; ?></span>
                        </div>
                    </div>
                </li>
                <?php } ?>
                <!-- End Resume timeline -->
            </ul>
        </div>
        <!-- End Resume Wrapper -->
    </div>
</article>