<article class="hs-content skills-section" id="section6">
    <span class="sec-icon fa fa-diamond"></span>
    <div class="hs-inner">
        <span class="before-title">.06</span>
        <h2>SKILLS</h2>
        <?php foreach ($allData['skills'] as $skillsData) { ?>

            <span class="content-title"><?php echo $skillsData['title']; ?></span>
            <div class="skolls">
                <span class="skill-description"><?php echo $skillsData['description']; ?></span>
                <div class="bar-main-container">
                    <div class="wrap">
                        <div class="bar-percentage" data-percentage="<?php 
                        if ($skillsData['level']     == 'Intermediate') { echo "50";}
                        elseif ($skillsData['level'] == 'Experts') { echo "60";}
                        elseif ($skillsData['level'] == 'Advance') { echo "80";}
                        elseif ($skillsData['level'] == 'Master') { echo "90";}
                        else { echo "40";}

                        ?>"></div>
                        <span class="skill-detail"><i class="fa fa-bar-chart"></i>LEVEL : <?php echo $skillsData['level']; ?></span><span class="skill-detail"><i class="fa fa-binoculars"></i>EXPERIENCE : <?php echo $skillsData['experience']; ?> YEARS</span>
                        <div class="bar-container">
                            <div class="bar"></div>
                        </div>

                        <?php 
                            $skillsField = explode(',',$skillsData['experience_area']);
                            foreach ($skillsField  as $fields) { ?>

                            <span class="label"><?php echo $fields; ?></span>

                        <?php } ?> 
                        <div style="clear:both;"></div>
                    </div>
                </div>
            </div>

     <?php } ?>  
    </div>
</article>