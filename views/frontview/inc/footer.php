 <div id="my-panel">
        </div>
        <!-- PLUGIN SCRIPTS -->
        <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../assets/js/default.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="../../assets/js/watch.js"></script>
        <script type="text/javascript" src="../../assets/js/layout.js"></script>
        <script type="text/javascript" src="../../assets/js/main.js"></script>
        <!-- END PLUGIN SCRIPTS -->
</body>

</html>
