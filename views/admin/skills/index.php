<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\skills\Skills;
$objskills = new Skills;
$allData = $objskills->index();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i>  <span class="text-semibold">MY SKILLS</span> || <a href="create.php">ADD NEW</a></h4>
			</div>
		</div>
	</div>
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
								  <?php
                                    if(isset($_SESSION['skmsg'])){
                                        echo "<h5 class='text-success'>".$_SESSION['skmsg']."</h5>";
                                        unset($_SESSION['skmsg']);
                                    }else{
                                    	echo "<h2 class='text-center'>Skill Information</h2>";
                                    } ?></th>
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Title</th>
									<th>Skill area</th>
									<th>Description</th>
									<th>Experience</th>
									<th>Level</th>
									<th>Category</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
							 	<?php $sl = 0 ;
							 		foreach ($allData as $data) { $sl++; ?>
									<tr>
										<td><?php  echo $sl; ?></td>
										<td><?php echo $data['title']; ?></td>
										<td><?php echo $data['experience_area']; ?></td>
										<td>
											<p class="text-justify"><?php echo $data['description']; ?></p>
										</td>
										<td><?php echo $data['experience']; if (!empty($data['experience'])) {echo " years";} ?> 
										</td>
										<td><?php echo $data['level']; ?></td>
										<td><?php echo $data['category']; ?></td>
										<td>
											<a class="btn-success" href="edit.php?id=<?php echo $data['unique_id']; ?>">Edit</a> 
										</td>									
										<td>
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="trash.php?id=<?php echo $data['unique_id']; ?>">Delete</a> 
										</td>
									</tr>							 			
							 	<?php	} ?>																
							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>