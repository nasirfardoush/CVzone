<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\awards\Awards;
$objawards = new Awards;


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ( !empty($_POST['title'])
			 	  AND !empty($_POST['organization'])
			 		  AND !empty($_POST['description'])
			 			   AND !empty($_POST['location'])
			 			 	 	   AND !empty($_POST['year'])
		 ){
			$objawards->setData($_POST)->update();
		}else{
			$_SESSION['awfail'] = "Awards information are not empty !";
			header('Location:create.php');
		}	
}

