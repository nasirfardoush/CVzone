  <?php
  if (!isset($_SESSION)) {session_start(); }
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">AWARDS - ADD</span> || 
					<a href="index.php">MY AWARDS</a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
  
		    <!-- about basic info about module -->
			<form action="store.php" method="POST" enctype="multipart/form-data">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
							<?php
									if(!isset($_SESSION['awmsg'])){
											echo "<h5>Please add your awards here .</h5>";

									}else{//Show Succesfull message
											  echo "<h5 class='text-success '>".$_SESSION['awmsg']."</h5>"; 
											  unset($_SESSION['awmsg']);	
								} ?>
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Awards Title</label>
										<input class="form-control input-xlg" type="tel" placeholder="Eb Developer" name="title">
									</div>			
									<div class="form-group">
										<label>Awards Year</label>
										<input id="datepicker" class="form-control" type="text" placeholder="2001" name="year">
									</div>										
									<div class="form-group">
										<label>Sort description</label>
										<textarea class="form-control"  placeholder="" name="description"></textarea>
									</div>									
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Organaizations Name</label>
										<input class="form-control input-xlg" type="text" placeholder="Webtech" name="organization">
									</div>					
									<div class="form-group">
										<label>Organaization Location</label>
										<input class="form-control input-xlg" type="text" placeholder="Bangladesh" name="location">
									</div>					
								</div>
							</div>
							<?php if(isset($_SESSION['awfail'])){
											  echo "<h5 class='text-danger'>".$_SESSION['awfail']."</h5>"; 
											  unset($_SESSION['awfail']);	
								} ?>
							<div class="form-group">
								<input class="marg-top" type="submit" value="Save" name="awards">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>