<?php
//Use class file here
include_once ('../../../vendor/autoload.php');
use App\admin\users\User;
$objUser = new User;
$allMembers 	= $objUser->getMemberIformation();
$allUsers 		= $objUser->getUserIformation();
$userByMonthly 	= $objUser->userByMonthly();
$usersByWeek 	= $objUser->userByWeekly();
$usersByDay 	= $objUser->usersByDay();
?>
<div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
	              <p class="count_top text-succes "><i class="fa fa-user"></i> USERS</p>
	              	<div class="count">
		              	<?php $totalUsers = count($allUsers);
		              	if (!empty($totalUsers)) {
		              		echo $totalUsers;}else{ echo 0; } ?>
	              		
	              	</div>
	              <p class="count_bottom">Total Resistered</p>
            </div>	            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
	              <p class="count_top text-succes "><i class="fa fa-user"></i>USERS</p>
	              <div class="count">
		              			<?php if (!empty($userByMonthly)) {
		              			 echo $userByMonthly;}else{ echo 0; } ?>
	              		
	              </div>
	              <p class="count_bottom">Registered In Month</p>
            </div>              
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
	              <p class="count_top text-succes "><i class="fa fa-user"></i>USERS</p>
	              	<div class="count">
		              <?php if (!empty($usersByWeek)) {
		               		echo $usersByWeek;}else{ echo 0; } ?>
	              		
	              	</div>
	              <p class="count_bottom">Registered In Week</p>
            </div>            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
	              <p class="count_top text-succes "><i class="fa fa-user"></i>USERS</p>
	              	<div class="count">
		              			<?php if (!empty($usersByDay)) {
		              			 echo $usersByDay;}else{ echo 0; } ?>
	              	 </div>
	              <p class="count_bottom">Registered Today</p>
            </div>

      </div>
		</div>
	</div>
	<!-- Users lists -->
	<div class="row">
		<div class="col-md-6">
			<!-- Collapsible list -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title usersList">ALL USERS</h5>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>

				<ul class="media-list media-list-linked">
				<?php $shasId = 0; foreach ($allUsers as $user) {  $shasId++; ?>
					<li class="media">
						<div class="media-link cursor-pointer" data-toggle="collapse" data-target="#u<?php echo $shasId;?>">
							<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle" alt=""></div>
							<div class="media-body">
								<div class="userImage">
									<img src="../../../assets/images/<?php echo $user['featured_img']; ?>">
								</div>								
								<div class="media-heading text-semibold userTitle">
									<p><?php echo $user['fullname'];?></p>
									<span><?php echo $user['created_at'];?></span>
								</div>
							</div>
							<div class="media-right media-middle text-nowrap">
								<i class="icon-menu7 display-block"></i>
							</div>
						</div>

						<div class="collapse" id="u<?php echo $shasId;?>">
							<div class="contact-details">
								<ul class="list-extended list-unstyled list-icons">
									<li><i class="icon-pin position-left"></i><?php echo $user['address'];?></li>
									<li><i class="icon-user-tie position-left"></i><?php echo $user['title'];?></li>
									<li><i class="icon-phone position-left"></i><?php echo $user['phone'];?></li>
									<li><i class="icon-mail5 position-left"></i><?php echo $user['email'];?></li>		
									<li><i class="icon-user-tie position-left"></i> <a href="../../frontview/?url=<?php echo $user['username'];?> "  target='_blank'>View profile</a></li>
									<li>
					<?php if ($user['user_role']== 2){
							 echo "<p class='text-success' >MEMBER".' '."|| <a href=''>BLOCK USER</a></p>";
							 }elseif($user['user_role']== 3){
								echo "<p class='text-success' >ADMIN</p>";
										}else{ ?>

											<a href="memberProcess.php?memberRequest=<?php echo $user['username'];?> ">ADD TO MEMBER</a> || <?php


											 if($user['user_role']== 1){ ?>
												<a href="memberProcess.php?blockRequest=<?php echo $user['username'];?>">BLOCK USER</a>;

												<?php }else{ ?>	
										<a href="memberProcess.php?unblockRequest=<?php echo $user['username'];?>">UNBLOCK</a>;
											 
									<?php } }?>


											 
									</li>
								</ul>
							</div>
						</div>
					</li>
					<?php } ?>		
				</ul>
			</div>
		</div>
		<!-- Users list -->


		<!-- Members list -->
		<div class="col-md-6">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title usersList">ALL MEMBER</h5>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>

				<ul class="media-list media-list-linked">
				<?php $shasId = 0; foreach ($allMembers as $member) {  $shasId++; ?>
					<li class="media">
						<div class="media-link cursor-pointer" data-toggle="collapse" data-target="#m<?php echo $shasId;?>">
							<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle" alt=""></div>
							<div class="media-body">
								<div class="userImage">
									<img src="../../../assets/images/<?php echo $member['featured_img']; ?>">
								</div>								
								<div class="media-heading text-semibold userTitle">
									<p><?php echo $member['fullname'];?></p>
									<span><?php echo $member['updated_at'];?></span>
								</div>
							</div>
							<div class="media-right media-middle text-nowrap">
								<i class="icon-menu7 display-block"></i>
							</div>
						</div>

						<div class="collapse" id="m<?php echo $shasId;?>">
							<div class="contact-details">
								<ul class="list-extended list-unstyled list-icons">
									<li><i class="icon-pin position-left"></i><?php echo $member['address'];?></li>
									<li><i class="icon-user-tie position-left"></i><?php echo $member['title'];?></li>
									<li><i class="icon-phone position-left"></i><?php echo $member['phone'];?></li>
									<li><i class="icon-mail5 position-left"></i><?php echo $member['email'];?></li>		
									<li><i class="icon-user-tie position-left"></i> <a href="../../frontview/?url=<?php echo$member['username'];?>" target='_blank'>View profile</a></li>
									<li>
										<a href="memberProcess.php?removeMember=<?php echo$member['username'];?>">REMOVE FROM MEMBER</a>
									</li>	
								</ul>
							</div>
						</div>
					</li>
					<?php } ?>		
				</ul>
			</div>
			<!-- /collapsible list -->
		</div>
	<!-- /members lists -->	
	</div>

</div>	