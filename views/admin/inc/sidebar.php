
<!-- Main sidebar -->
<div class="sidebar sidebar-main">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user">
			<div class="category-content">
				<div class="media">
					<a href="#" class="media-left"><img src="../../../assets/images/<?php echo $settData['featured_img'];?>" class="img-circle img-sm" alt=""></a>
					<div class="media-body">
						<span class="media-heading text-semibold"><?php echo $_SESSION['user']['first_name']." ".$_SESSION['user']['last_name'];?></span>
						<div class="text-size-mini text-muted">
							<i class="icon-pin text-size-small"></i> &nbsp;<?php echo  $settData['address'];;?>
						</div>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list">
							<li>
								<a href="../settings/edit.php"><i class="icon-cog3"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="../users/dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>SETTINGS</span></a>
									<ul>									
										<li>
											<a href="../settings/edit.php">Manage Account Settings</a>
										</li>										
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>ABOUT</span></a>
									<ul>									
										<li>
											<a href="../abouts/index.php">Manage Abouts</a>
										</li>										
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>HOBBIES</span></a>
									<ul>
										<li>
											<a href="../hobbies/create.php">Add new</a>
										</li>										
										<li>
											<a href="../hobbies/index.php">Manage Hobbies</a>
										</li>										
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>FACTS</span></a>
									<ul>
										<li>
											<a href="../facts/create.php">Add new</a>
										</li>										
										<li>
											<a href="../facts/index.php">Manage Facts</a>
										</li>										
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>EDUCATIONS</span></a>
									<ul>
										<li>
											<a href="../educations/create.php">Add new</a>
										</li>
										<li>
											<a href="../educations/index.php">Manage Educations</a>
										</li>
									</ul>
								</li>									
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>EXPREINCE</span></a>
									<ul>
										<li>
											<a href="../expreinces/create.php">Add new</a>
										</li>
										<li>
											<a href="../expreinces/index.php">Manage Educations</a>
										</li>
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>AWARDS</span></a>
									<ul>
										<li>
											<a href="../awards/create.php">Add new</a>
										</li>
										<li>
											<a href="../awards/index.php">Manage Awards</a>
										</li>
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>POST</span></a>
									<ul>
										<li><a href="../posts/create.php">Add new</a></li>
										<li><a href="../posts/index.php">Manage Post</a></li>
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>SERVICES</span></a>
									<ul>
										<li>
											<a href="../services/create.php">Add New</a>
										</li>										
										<li>
											<a href="../services/index.php">Manage services</a>
										</li>
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>TEACHING</span></a>
									<ul>
										<li>
											<a href="../teaching/create.php">Add New</a>
										</li>										
										<li>
											<a href="../teaching/index.php">Manage Teaching</a>
										</li>
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>SKILLS</span></a>
									<ul>
										<li>
											<a href="../skills/create.php">Add New</a>
										</li>
										<li>
											<a href="../skills/index.php">Manage Skills</a>
										</li>
									</ul>
								</li>								
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>PORTFOLIO</span></a>
									<ul>
										<li><a href="../portfolios/create.php">Add New</a></li>
										<li><a href="../portfolios/index.php">Manage portfolio</a></li>
									</ul>
								</li>								
								<li>
									<a href="../Contacts/index.php"><i class="icon-stack2"></i> <span>CONTACT</span></a>

								</li>

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
