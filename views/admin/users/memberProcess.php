<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\users\User;
$objuser = new User;
if (isset($_GET['memberRequest'])) {
	$objuser->setMember($_GET);

}elseif(isset($_GET['removeMember'])){
	$objuser->removeMember($_GET);
}
elseif(isset($_GET['blockRequest'])){
	$objuser->blockedMember($_GET);
}elseif(isset($_GET['unblockRequest'])){
	$objuser->unblockMember($_GET);
}
?>