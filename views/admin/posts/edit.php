<?php
include_once ('../../../vendor/autoload.php');
use App\admin\posts\Posts;
$objpost = new Posts();
$data = $objpost->setData($_GET)->show();


include_once('../inc/header.php');
include_once('../inc/sidebar.php');
?>

<!-- Main content -->
<div class="content-wrapper">
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">POSTS - EDIT</span> || <a href="index.php"> MY POSTS </a> </h4>
            </div>
        </div>
    </div>
    <!-- Add about terms -->
    <div class="row ">
        <!-- about basic info about module -->
        <form action="update.php" method="POST" enctype="multipart/form-data">
            <fieldset class="content-group">
                <div class="form-group">
                    <div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
                            <?php
                            if(!isset($_SESSION['pmsg'])){
                                echo "<h5>You can Update  your post .</h5>";

                            }else{//Show Succesfull message
                                echo "<h5 class='text-success'>".$_SESSION['pmsg']."</h5>";
                                unset($_SESSION['pmsg']);
                            }	?>
                            <!-- section one -->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Posts Title</label>
                                    <input class="form-control " type="tel" placeholder="Learn PHP" value="<?php echo $data['title']; ?>" name="title">
                                </div>
                                <div class="form-group">
                                    <label>Author name</label>
                                    <input  class="form-control" type="text" placeholder="Rahim" value="<?php echo $data['author_name']; ?>" name="author_name">
                                </div>
                                <div class="form-group">
                                    <label>Categoty name</label>
                                    <input  class="form-control" type="text" placeholder="Education" value="<?php echo $data['categories']; ?>" name="categories">
                                </div>

                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control input-xlg"  placeholder="Description"  name="description" ><?php echo $data['description']; ?></textarea>
                                </div>
                            </div>
                            <!-- Second section -->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Country Name</label>
                                    <input class="form-control" type="text" placeholder="Dhaka" value="<?php echo $data['country_name']; ?> " name="country_name">
                                </div>
                                <div class="form-group">
                                    <label>City name</label>
                                    <input class="form-control" type="text" placeholder="Bangladesh" value="<?php echo $data['city_name']; ?>" name="city_name">
                                </div>
                                <div class="form-group">
                                    <label>Tags</label>
                                    <input class="form-control" type="text" placeholder="php" value="<?php echo $data['tags']; ?>" name="tags">
                                </div>                                
                                <div class="form-group">
                                    <label>Post Image</label>
                                    <input class="form-control" type="file"  name="img">
                                    <img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/<?php echo $data['img']; ?>" alt="No Image">

                                </div>
                            </div>
                        </div>
                        <?php //Show empty message
                        if(isset($_SESSION['pfail'])){
                            echo "<h5 class='text-danger'>".$_SESSION['pfail']."</h5>";
                            unset($_SESSION['pfail']);
                        }	?>

                        <div class="form-group">
                            <input type="hidden" value="<?php echo $data['id']; ?>" name="id">
                            <input type="submit" value="Update" name="aboutupdate">
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>