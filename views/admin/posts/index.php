<?php
include_once ('../../../vendor/autoload.php');
use App\admin\posts\Posts;
$objpost = new Posts;
$allData = $objpost->index();

include_once('../inc/header.php');
include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">MY POSTS </span> || <a href="create.php">ADD NEW</a></h4>
			</div>
		</div>
	</div>
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10">
                                    <?php
                                    if(isset($_SESSION['pmsg'])){
                                        echo "<h5 class='text-success'>".$_SESSION['pmsg']."</h5>";
                                        unset($_SESSION['pmsg']);
                                    }else{
                                    	echo "<h2 class='text-center'>Posts Information</h2>";
                                    	} ?>
                                    	</th>
								</tr>

								<tr>
									<th>Image</th>
									<th>Title</th>
									<th>Description</th>
									<th>Category name</th>
									<th>Tags</th>
									<th>Author name</th>
									<th>City</th>
									<th>Country</th>
									<th colspan="3">Manage</th>
								</tr>
                                <?php foreach ($allData as $data) { ?>
							</thead>
							<tbody>
								<tr>
									<td><img  width="60" height="50" src="../../../assets/images/<?php echo $data['img']; ?>"> </td>
									<td><?php echo $data['title']; ?></td>
									<td >
										<p class="text-justify"><?php echo substr($data['description'], 0,145); ?>....</p>
									</td>
									<td><?php echo $data['categories']; ?></td>
									<td><?php echo $data['tags']; ?></td>
									<td><?php echo $data['author_name']; ?></td>
									<td><?php echo $data['city_name']; ?></td>
									<td><?php echo $data['country_name']; ?></td>
									<td>
                                        <a class="btn-success" href="show.php?id=<?php echo $data['unique_id']; ?>">View</a>
									</td>
									<td>
                                        <a class="btn-success" href="edit.php?id=<?php echo $data['unique_id']; ?>">Edit</a>
                                      </td>  
									<td>
										<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="trash.php?id=<?php echo $data['unique_id'];?>">Delete</a>
									</td>
								</tr>								
								<?php }?>

							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>