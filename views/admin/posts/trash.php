<?php
include_once ('../../../vendor/autoload.php');
use App\admin\posts\Posts;
$objpost = new Posts;

if (isset($_GET['id'])) {
    $objpost->setData($_GET)->softDelete();
}