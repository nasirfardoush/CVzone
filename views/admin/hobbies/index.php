  <?php
 include_once ('../../../vendor/autoload.php');
use App\admin\hobbies\Hobbies;
$objhobbies = new Hobbies;
$allData = $objhobbies->index();


 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">MY HOBBIES || <a href="create.php">ADD NEW</a></span></h4>
			</div>
		</div>
	</div>
	<!-- View hobbies options -->	
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="4">
									 <?php
										if(isset($_SESSION['hmsg'])){
												echo "<h3 class='text-success'>".$_SESSION['hmsg']."</h3>"; 
											     unset($_SESSION['hmsg']);	
										} else{
												echo "<h2 class='text-center'>Hobbies</h2>";
										}	?>	
									
									</th>
								</tr>				
								<tr>
									<th>Image</th>
									<th>Title</th>
									<th>Description</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									foreach ($allData as  $data) { ?>
										<tr>
											<td><img width="90" height="70" src="../../../assets/images/<?php echo $data['img']; ?>" alt="No Image"> 
											</td>
											<td>
												<?php echo $data['title']; ?>
											</td>
											<td><p class="text-justify">
												<?php echo $data['description']; ?>
											</p></td>
											<td>
												<a class="btn-success" href="edit.php?id=<?php echo $data['unique_id']; ?>">Edit</a>
											</td>
											<td>	
												<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="trash.php?id=<?php echo $data['unique_id']; ?>">Delete</a> 
											</td>
										</tr>									
								<?php } ?>
							</tbody>
						</table>
				</div>
		 </div>
	</div>			 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>