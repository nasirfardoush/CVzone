  <?php
include_once ('../../../vendor/autoload.php');
use App\admin\hobbies\Hobbies;
$objhobbies = new Hobbies;
$data = $objhobbies->setData($_GET)->show();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');
?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">HOBBIES - EDIT</span> || <a href="index.php"> MY HOBBIES </a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
		<!-- Add hobbies -->
		<form action="update.php" method="POST" enctype="multipart/form-data">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
								<h2>You can Edit your hobbies .</h2>
								<div class="form-group">
									<label>Hobbies title</label>
									<input class="form-control input-xlg" type="text" value="<?php echo $data['title']; ?>" name="title">
								</div>									
								<div class="form-group">
									<label>Hobbies description</label>
									<input class="form-control input-xlg" type="text" value="<?php echo $data['description']; ?>" name="description">
								</div>								
								<div class="form-group">
									<label>Hobbies image</label>
									<input class="form-control input-xlg" type="file" value="<?php echo $data['img']?>" name="img">
									<img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/<?php echo $data['img']; ?>" alt="No Image">
																 <?php
										if(isset($_SESSION['hfail'])){
											echo "<h5 class='text-danger'>".$_SESSION['hfail']."</h5>"; 
											  unset($_SESSION['hfail']);

										}?>
								</div>					
								<div class="form-group">
									<input class="input-xs" type="hidden" value="<?php echo $data['id']?>" name="id">
									<input class="input-xs" type="submit" value="Update" name="hobbies">

								</div>
							</div>
							</div>
						</div>
				</div>
			</fieldset>
		</form>

	 </div>
  </div>	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>