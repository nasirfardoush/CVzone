<?php
//Use class file here
include_once ('../../../vendor/autoload.php');
use App\admin\abouts\About;
$objabout = new About;
$data = $objabout->show();

//Include class file here	 
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php'); ?>
<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">ABOUT</span></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
		    <!-- about basic info about module -->
			<form action="update.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10">
							<div class="row">
								<div class="col-md-6 col-md-offset-1">							
									<div class="form-group">
										<label>Phone number</label>
										<input class="form-control input-xlg" type="tel" value="<?php echo $data['phone']; ?>" name="phone">
									</div>					
									<div class="form-group">
										<label>Skills work area</label>
										<input class="form-control input-xlg" type="text" value="<?php echo $data['work_area']; ?>" name="work_area">
										<small>Add another work area and separeted with comma(,) .</small>
									</div>

									<div class="form-group">
										<label>Sort description about you</label>
										<input class="form-control input-lg" type="text" value="<?php echo $data['short_desc']; ?>" name="short_desc">
									</div>									
									<div class="form-group">
										<label>BIO</label>
										<textarea class="form-control input-lg" name="bio"> <?php echo $data['bio']; ?></textarea>
									</div>
									 <?php //Show empty message
										if(isset($_SESSION['afail'])){ 
											  echo "<h5 class='text-danger'>".$_SESSION['fail']."</h5>"; 
											  unset($_SESSION['afail']);	
									}	?>									
									<div class="form-group">
										<input type="hidden" value="<?php echo $data['user_id']; ?>" name="id">
										<input type="submit" value="Udate" name="aboutupdate">
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</form>
	 	</div>
   </div>	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>