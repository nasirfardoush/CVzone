<?php
include_once ('../../../vendor/autoload.php');
use App\admin\teaching\Teaching;
$objteaching = new Teaching;
$data = $objteaching->setData($_GET)->show();

include_once('../inc/header.php');
include_once('../inc/sidebar.php');

?>
<!-- Main content -->
<div class="content-wrapper">
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">TEACHING - EDIT</span> || <a href="index.php">MY TEACHING</a>
                </h4>
            </div>
        </div>
    </div>
    <!-- Add about terms -->
    <div class="row ">

        <!-- Teaching Module -->
        <form action="update.php" method="POST">
            <fieldset class="content-group">
                <div class="form-group">
                    <div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="row">
                            <?php
                            if(!isset($_SESSION['temsg'])){
                                echo "<h5>You can update  your teaching .</h5>";

                            }else{//Show Succesfull message
                                echo "<h5 class='text-success'>".$_SESSION['temsg']."</h5>";
                                unset($_SESSION['temsg']);
                            }	?>
                            <!-- section one -->
                            <div class="col-md-5">
                                <div class="form-group">

                                    <label>Title</label>
                                    <input class="form-control input-xlg" type="text" placeholder="Professor"  value="<?php echo $data['title']; ?>" name="title">
                                </div>
                                <div class="form-group">
                                    <label>Start Year</label>
                                    <input id="datepicker" class="form-control" type="text"  value="<?php echo $data['start_date']; ?>" placeholder="2001" name="start_date">
                                </div>
                                <div class="form-group">
                                    <label>Sort description</label>
                                    <textarea class="form-control"  placeholder=""  name="teaching_desc"><?php echo $data['teaching_desc']; ?></textarea>
                                </div>
                            </div>
                            <!-- Second section -->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Institute Name</label>
                                    <input class="form-control input-xlg" type="text" placeholder="Dhaka university"  value="<?php echo $data['institute']; ?>" name="institute">
                                </div>
                                <div class="form-group">
                                    <label>End Year</label>
                                    <input id="datepicker2" class="form-control" type="text" placeholder="2010"  value="<?php echo $data['end_date']; ?>" name="end_date">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control"  name="teaching_status">
                                        <?php if (!empty($data['teaching_status'])) { ?>
                                        <option value="<?php echo $data['teaching_status']; ?>"><?php echo $data['teaching_status']; ?>
                                        </option>
                                        <?php } ?>                                     
                                        <option name="teaching_status"  value="CURRENT">Curent</option>
                                        <option name="teaching_status" value="PREVIOUS">Privious</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php //Show empty message
                        if(isset($_SESSION['tefail'])){
                            echo "<h5 class='text-danger'>".$_SESSION['tefail']."</h5>";
                            unset($_SESSION['tefail']);
                        }	?>
                        <div class="form-group">
                            <input type="hidden" value="<?php echo $data['id']; ?>" name="id">
                            <input class="marg-top" type="submit" value="Update" name="teaching">
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>