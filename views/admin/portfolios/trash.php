<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\portfolios\Portfolios;
$objportfolio = new Portfolios;

if (isset($_GET['id'])) {
	$objportfolio->setData($_GET)->softDelete();
}