<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\portfolios\Portfolios;
$objportfolio = new Portfolios;
$data = $objportfolio->setData($_GET)->show();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">PORTFOLIOS - EDIT</span> || <a href="index.php"> MY PORTFOLIOS </a></h4>
			</div>
		</div>
	</div>
		<!-- Add facts -->
		<form action="update.php" method="POST" enctype="multipart/form-data">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
							 <?php
										if(!isset($_SESSION['pomsg'])){
											echo "<h5>Please add your facts here .</h5>";

										}else{//Show Succesfull message
											  echo "<h5 class='text-success'>".$_SESSION['pomsg']."</h5>"; 
											  unset($_SESSION['pomsg']);	
									}	?>
								<div class="form-group">
									<label>Project Title</label>
									<input class="form-control" type="text" value="<?php echo $data['title']; ?>" name="title">
								</div>														
								<div class="form-group">
									<label>Category</label>
									<input class="form-control" type="text" value="<?php echo $data['category']; ?>" name="category">

								</div>								
								<div class="form-group">
									<label>Project description</label>
									<textarea class="form-control input-xlg" name="description" >
										<?php echo $data['description']; ?>
									</textarea>
								</div>
								<div class="form-group">
									<label>Project image</label>
									<input class="form-control" type="file" name="img" >
									<img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/<?php echo $data['img']; ?>" alt="No Image">
								</div>	
								<?php //Show empty message
										if(isset($_SESSION['pofail'])){ 
											  echo "<h5 class='text-danger'>".$_SESSION['pofail']."</h5>"; 
											  unset($_SESSION['pofail']);	
									}	?>
								<div class="form-group">
									<input class="input-xs" type="hidden" value="<?php echo $data['id']; ?>" name="id">
									<input class="input-xs" type="submit" value="Update" name="protfolios">

								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	 </div>
    </div>
   </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>