<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\portfolios\Portfolios;
$objportfolio = new Portfolios;
$data = $objportfolio->setData($_GET)->show();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">PORTFOLIOS - VIEW DETAILS </span> || <a href="create.php">ADD NEW</a> || <a href="index.php">MY PORTFOLIOS</a></h4>
			</div>
		</div>
	</div>
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10"><h2 class="text-center">Service events</h2></th>
								</tr>				
								<tr>
									<th>Project image</th>
									<th>Project Title</th>
									<th>Category</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
									<img width="90" height="70" src="../../../assets/images/<?php echo $data['img']; ?>" alt="No Image"> 
									</td>
									<td><?php echo $data['title']; ?></td>
									<td><?php echo $data['category']; ?></td>
								</tr>
								<tr>
									<td  colspan="3">
										<label>Project description :</label>
										<p class="text-justify"><?php echo $data['description']; ?></p>
									</td>									
								</tr>
								<tr>
									<td colspan="3">									
										<a class="btn-success" href="edit.php?id=<?php echo $data['unique_id']; ?>">Edit</a> ||
										<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="trash.php?id=<?php echo $data['unique_id']; ?>">Delete</a> 
									</td>
								</tr>															
							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>