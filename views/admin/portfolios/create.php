  <?php
  if (!isset($_SESSION)) {session_start(); }
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">PORTFOLIOS - ADD</span> || <a href="index.php"> MY PORTFOLIOS </a></h4>
			</div>
		</div>
	</div>
		<!-- Add facts -->
		<form action="store.php" method="POST" enctype="multipart/form-data">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
							 <?php
										if(!isset($_SESSION['pomsg'])){
											echo "<h5>Please add your portfolios here .</h5>";

										}else{//Show Succesfull message
											  echo "<h5 class='text-success'>".$_SESSION['pomsg']."</h5>"; 
											  unset($_SESSION['pomsg']);	
									}	?>
								<div class="form-group">
									<label>Project Title</label>
									<input class="form-control" type="text" placeholder="Web Design" name="title">
								</div>	
								<div class="form-group">
									<label>Project image</label>
									<input class="form-control" type="file" name="img" >
								</div>														
								<div class="form-group">
									<label>Category</label>
									<input class="form-control" type="text" placeholder="Logo design" name="category">

								</div>								
								<div class="form-group">
									<label>Project description</label>
									<textarea class="form-control input-xlg" name="description" ></textarea>
								</div>
								<?php //Show empty message
										if(isset($_SESSION['pofail'])){ 
											  echo "<h5 class='text-danger'>".$_SESSION['pofail']."</h5>"; 
											  unset($_SESSION['pofail']);	
									}	?>
								<div class="form-group">
									<input class="input-xs" type="submit" value="Save" name="protfolios">

								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	 </div>
    </div>
   </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>