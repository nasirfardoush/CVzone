<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\educations\Education;
$objeduc = new Education;
$allData = $objeduc->index();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">MY EDUCATIONS</span> || <a href="create.php">ADD NEW</a></h4>
			</div>
		</div>
	</div>
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="12"><h2 class="text-center">Education Information</h2></th>
								 <?php
									if(isset($_SESSION['edmsg'])){
											echo "<h5 class='text-success '>".$_SESSION['edmsg']."</h5>";
											 unset($_SESSION['edmsg']);	
									}?>
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Title</th>
									<th>Degree</th>
									<th>Institute</th>
									<th>Institute location</th>
									<th>Enrolled year</th>
									<th>Pasing year</th>
									<th>Result(GPA)</th>
									<th>Duration</th>
									<th>Board</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 0;
									foreach ($allData as $data) {$i++; ?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $data['title'];  ?></td>
										<td><?php echo $data['degree']; ?></td>
										<td><?php echo $data['institute']; ?></td>
										<td><?php echo $data['location']; ?></td>
										<td><?php echo $data['enrolled_year']; ?></td>
										<td><?php echo $data['passing_year']; ?></td>
										<td><?php echo $data['result']; ?></td>
										<td><?php echo $data['course_duration']; ?></td>
										<td><?php echo $data['education_board']; ?></td>
										<td>
											<a class="btn-success" href="edit.php?id=<?php echo $data['unique_id']; ?>">Edit</a> 
										</td>									
										<td>
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="trash.php?id=<?php echo $data['unique_id']; ?>">Delete</a> 
										</td>
									</tr>	
								<?php } ?>														
							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>