  <?php
  session_start();
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">EDUCATIONS - ADD</span> || <a href="index.php">MY EDUCATIONS</a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
  
		    <!-- about basic info about module -->
			<form action="store.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
							 <?php
									if(!isset($_SESSION['edmsg'])){
											echo "<h5>Please add your educations here .</h5>";

									}else{//Show Succesfull message
											  echo "<h5 class='text-success '>".$_SESSION['edmsg']."</h5>"; 
											  unset($_SESSION['edmsg']);	
								} ?>
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Educations Title</label>
										<input class="form-control" type="tel" placeholder="Computer engineering" name="title">
									</div>					
									<div class="form-group">
										<label>Institute Name</label>
										<input class="form-control" type="text" placeholder="University of Dhaka" name="institute">
									</div>

									<div class="form-group">
										<label>Enrolled Year</label>
										<input id="datepicker" class="form-control" type="text" placeholder="2001" name="enrolled_year">
									</div>									
									<div class="form-group">
										<label>Result</label>
										<input class="form-control" type="text" placeholder="4.00" name="result">
										<small>Enter result following GPA Standart</small>
									</div>									
									<div class="form-group">
										<label>Board</label>
										<input class="form-control" type="text" placeholder="Technical" name="education_board">
									</div>
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Educations Degree</label>
										<input class="form-control" type="tel" placeholder="MSC or BSc..." name="degree">
										<small>Please write sort name.</small>
									</div>					
									<div class="form-group">
										<label>Institute Location</label>
										<input class="form-control" type="text" placeholder="Bangladesh" name="location">
									</div>

									<div class="form-group">
										<label>Passing Year</label>
										<input id="datepicker2" class="form-control" type="text" placeholder="2005" name="passing_year">
									</div>									
									<div class="form-group">
										<label>Course duration(Years)</label>
										<input class="form-control" type="text" placeholder="4" name="course_duration">
									</div>																	
								</div>
							</div>
								<?php if(isset($_SESSION['edfail'])){
											  echo "<h5 class='text-danger'>".$_SESSION['edfail']."</h5>"; 
											  unset($_SESSION['edfail']);	
								} ?>
							<div class="form-group">
								<input class="marg-top" type="submit" value="Save" name="education">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>