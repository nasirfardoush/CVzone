<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\settings\Settings;
$objset = new Settings;
$data = $objset->show();
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');
?>


<!-- Main content -->
<div class="content-wrapper">

	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Settings</span></h4>
			</div>
		</div>
	</div>
	<!--Setting options code-->
	<form action="update.php" method="POST" enctype="multipart/form-data">
		<fieldset class="content-group">
		<div class="form-group">
			<div class="col-lg-10">
				<div class="row">
					<div class="col-md-6 col-md-offset-1">
						<?php	if(!isset($_SESSION['smsg'])){
											echo "<h5>You can change your account settings .</h5>";

									}else{//Show Succesfull message
											  echo "<h5 class='text-success '>".$_SESSION['smsg']."</h5>"; 
											  unset($_SESSION['smsg']);	
								} ?>
						<div class="form-group">
							<label>Profile Picture</label>
							<input class="form-control" type="file"  name="featured_img">
						</div>					
						<div class="form-group">
							<label>Your title</label>
							<input class="form-control" type="text" value="<?php echo $data['title']; ?>" name="title">
						</div>

						<div class="form-group">
							<label>Your name</label>
							<input class="form-control" type="text" value="<?php echo $data['fullname']; ?>" name="fullname">
						</div>
						<div class="form-group">
						<label>Curent Location</label>
							<input class="form-control input-sm" type="text" value="<?php echo $data['address']; ?>" name="address">
						</div>
						<div class="form-group">
							<h3>Chose your Theme color</h3>
							<input type="radio" <?php echo ($data['themecolor']=='blue')?'checked':false; ?> value="blue" name="themecolor">
							<label>Blue</label> 
							<input type="radio" <?php echo($data['themecolor']=='brown')?'checked':false; ?> value="brown" name="themecolor">
							<label>Brown</label>
							<input type="radio" <?php echo ($data['themecolor']=='cyan')?'checked':false; ?> value="cyan" name="themecolor">
							<label>Cyan</label>
							<input type="radio" <?php echo ($data['themecolor']=='light-green')?'checked':false; ?> value="light-green" name="themecolor">
							<label>Light Green</label>						
							<input type="radio" <?php echo ($data['themecolor']=='orange')?'checked':false; ?> value="orange" name="themecolor">
							<label>Orange</label>
							<input type="radio" <?php echo ($data['themecolor']=='green')?'checked':false; ?> value="green" name="themecolor">
							<label>Default</label>

							<?php	if(isset($_SESSION['sfail'])){										
								 echo "<h5 class='text-danger '>".$_SESSION['sfail']."</h5>"; 
								 unset($_SESSION['sfail']);		} ?>
						</div>
						<div class="form-group">
							<input class="input-xs" type="hidden" value="<?php echo $data['id']; ?>" name="id">
							<input class="input-xs" type="submit" value="Save new change">
						</div>
					</div>				
					<div class="col-md-4 col-lg-4 ">
						<h1></h1>
						<img width="100" height="90" src="../../../assets/images/<?php echo $data['featured_img']; ?>" alt="Profile Image!!!">
					</div>	
				</div>
			</div>
		</fieldset>
	</form>
	<!--Setting options code-->
</div>	
<!-- /main content -->

<?php include_once('inc/footer.php'); ?>