<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\settings\Settings;
$objset = new Settings;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		if (!empty($_FILES['featured_img']['name'])) {
			$alow    = array('jpg','jpeg','png');    
			$file_name      = $_FILES['featured_img']['name'];    
			$file_size      = $_FILES['featured_img']['size'];     
			$file_path      = $_FILES['featured_img']['tmp_name'];    
			$explodeExt     = explode('.', $file_name);    
			$file_ext   	= strtolower(end($explodeExt));    
			$file_unique_name = substr(md5(time()), 0,7).'.'.$file_ext;    
			$file_storage ="../../../assets/images/".$file_unique_name;

			if(in_array($file_ext , $alow )===false) {         
				$_SESSION['fail'] = "You can upload only".implode(", ", $alow); 				
				header('Location:create.php');
    
			}else{ 
				 move_uploaded_file($file_path , $file_storage);
				 $_POST['featured_img'] = $file_unique_name;
				 $objset->setData($_POST)->update();
			}
		}else{
			$objset->setData($_POST)->update();
		}

	}

