<?php 
include_once ('../../../vendor/autoload.php');
 use App\admin\contacts\Contact;
 $objscontact = new Contact;
$data = $objscontact->setData($_GET)->show();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
		<div class="panel panel-default">
		  <!-- Default panel contents -->
		  <div class="panel-heading text-center ">All Message List</div>	

			<form action="mail.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<!-- section one -->
								<div class="col-md-6">
									<div class="form-group">
										<label>Reply TO</label>
										<input class="form-control" readonly="" value="<?php echo $data['email']; ?>" type="email"  name="tomail">
									</div>										
									<div class="form-group">
										<label>Form</label>
										<input class="form-control" type="email"  name="frommail">
									</div>										
									<div class="form-group">
										<label>Subject</label>
										<input class="form-control" type="text"  name="subject">
									</div>										
									<div class="form-group">
										<label>Message</label>
										<textarea class="form-control" name="message">
										</textarea>
									</div>					

							<div class="form-group">
								<input class="marg-top" type="submit" value="SEND" name="reply">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
		</div>
	</div>		 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>
