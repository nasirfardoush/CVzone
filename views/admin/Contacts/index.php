<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\contacts\Contact;
$objscontact = new Contact;
$allData = $objscontact->index();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="panel panel-default">
		  <!-- Default panel contents -->
		  <div class="panel-heading  "><h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">MY CONTACTS</span></h4></div>
				<div class="table-responsive">
		<table class="table bg-slate-600">
			<thead>
		    	<tr>
		    		<th>Sl.</th>
		    		<th>Name </th>
		    		<th>Email</th>
		    		<th>Message</th>
		    		<th>Action</th>
		    	</tr>	
		    	</thead>
		    	<tbody>
		    	<?php $sl = 0;
		    		foreach ($allData as $data) {$sl++; ?>    			    		
			    		<tr class="<?php echo ($data['status']==0)?'text-success':false;?>" >
			    			<td><?php echo $sl; ?></td>
			    			<td><?php echo $data['name'];?></td>
			    			<td><?php echo $data['email'];?></td>
			    			<td>
			    				<p class="text-justify" > <?php 
			    					$message = substr($data['message'], 0,100);
			    				echo $message."......."; ?></p>
			    			</td>
			    		<td>
			    			<a href="show.php?id=<?php echo $data['unique_id']; ?>">View</a>||
			    			<a href="reply.php?id=<?php echo $data['unique_id']; ?>">Reply</a>||
			    			<a href="trash.php?id=<?php echo $data['unique_id']; ?>">Delete</a>
			    		</td>
			    	</tr>
		    	<?php } ?>	
		    	</tbody>
			</table>
		</div>
	</div>		 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>
