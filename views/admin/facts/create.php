  <?php
  if (!isset($_SESSION)) {	session_start(); }
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i>FACTS - ADD || <a href="index.php"> MY FACTS </a></h4>
			</div>
		</div>
	</div>
		<!-- Add facts -->
		<form action="store.php" method="POST" enctype="multipart/form-data">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
							 <?php
										if(!isset($_SESSION['fmsg'])){
											echo "<h5>Please add your facts here .</h5>";

										}else{//Show Succesfull message
											  echo "<h5 class='text-success'>".$_SESSION['fmsg']."</h5>"; 
											  unset($_SESSION['fmsg']);	
									}	?>
								<div class="form-group">
									<label>Facts Title</label>
									<input class="form-control input-xlg" type="text" placeholder="Web Design" name="title">
								</div>	
								<div class="form-group">
									<label>Facts image</label>
									<input class="form-control input-xlg" type="file" name="img" >
								</div>														
								<div class="form-group">
									<label>Facts number</label>
									<input class="form-control input-xlg" type="text" placeholder="52" name="no_of_items">
									<?php //Show empty message
										if(isset($_SESSION['ffail'])){ 
											  echo "<h5 class='text-danger'>".$_SESSION['ffail']."</h5>"; 
											  unset($_SESSION['ffail']);	
									}	?>
								</div>
								<div class="form-group">
									<input class="input-xs" type="submit" value="Save" name="facts">

								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	 </div>
    </div>
   </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>