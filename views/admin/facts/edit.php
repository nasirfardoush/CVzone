<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\facts\Facts;
$objfacts = new Facts;
$data = $objfacts->setData($_GET)->show();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">FACTS - EDIT </span>|| <a href="index.php">MY FACTS</a></h4>
			</div>
		</div>
	</div>
		<!-- Add facts -->
		<form action="update.php" method="POST" enctype="multipart/form-data">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
								<h5>You can update your facts .</h5>;
								<div class="form-group">
									<label>Facts Title</label>
									<input class="form-control input-xlg" type="text" value="<?php echo $data['title']?>" name="title">
								</div>														
								<div class="form-group">
									<label>Facts number</label>
									<input class="form-control input-xlg" type="text" value="<?php echo $data['no_of_items']?>"" name="no_of_items">
								</div>
								<div class="form-group">
									<label>Facts image</label>
									<input class="form-control input-xlg" type="file" name="img" >
								</div>	
								<?php //Show empty message
										if(isset($_SESSION['ffail'])){ 
											  echo "<h5 class='text-danger'>".$_SESSION['ffail']."</h5>"; 
											  unset($_SESSION['ffail']);	
									}	?>
								<div class="form-group">
									<input class="input-xs" type="hidden" value="<?php echo $data['id']?>" name="id">
									<input class="input-xs" type="submit" value="Update" name="facts">

								</div>

							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	 </div>
    </div>
   </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>