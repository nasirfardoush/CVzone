<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\services\Services;
$objservices = new Services;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if ($_POST['description']==''AND $_POST['title']=='' ) {
		$_SESSION['fail'] = "Description and Title must be provide!";
			header('Location:edit.php');
				exit();
	}else{
		if (!empty($_FILES['img']['name'])) {
			//services image
			$alow    		= array('jpg','jpeg','png');    
			$file_name      = $_FILES['img']['name'];    
			$file_size      = $_FILES['img']['size'];     
			$file_path      = $_FILES['img']['tmp_name'];    
			$explodeExt     = explode('.', $file_name);    
			$file_ext   	= strtolower(end($explodeExt));    
			$file_unique_name = substr(md5(time()), 0,7).'.'.$file_ext;    
			$file_storage ="../../../assets/images/".$file_unique_name;

			if(in_array($file_ext , $alow )===false) {         
				$_SESSION['serfail'] = "You can upload only ".implode(", ", $alow);
				header('Location:edit.php');     
			}else{ 
				 move_uploaded_file($file_path , $file_storage);
				  $img = $file_unique_name;
			}
			//client image
			$alow    		= array('jpg','jpeg','png');    
			$file_name      = $_FILES['client_image']['name'];    
			$file_size      = $_FILES['client_image']['size'];     
			$file_path      = $_FILES['client_image']['tmp_name'];    
			$explodeExt     = explode('.', $file_name);    
			$file_ext   	= strtolower(end($explodeExt));    
			$file_unique_name = 'client'.substr(md5(time()), 0,5).'.'.$file_ext;    
			$file_storage ="../../../assets/images/".$file_unique_name;

			if(in_array($file_ext , $alow )===false) {         
				$_SESSION['serfail'] = "You can upload only ".implode(", ", $alow);
				header('Location:edit.php');   
			}else{ 
				 move_uploaded_file($file_path , $file_storage);
				 $client_image = $file_unique_name;
				 //store data			
				$_POST['img'] = $img;
				$_POST['client_image'] = $client_image;
				$objservices->setData($_POST)->update();
			}
	 	}else{
	 		$objservices->setData($_POST)->update();
	 	}
	}
}
