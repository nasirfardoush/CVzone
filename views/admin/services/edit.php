<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\services\Services;
$objservices = new Services;
$data = $objservices->setData($_GET)->show();


 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">SERVICES - EDIT</span> || <a href="index.php">MY SERVICES</a></h4>
			</div>
		</div>
	</div>
<!-- Add Services terms -->
	<div class="row ">
			<form action="update.php" method="POST"  enctype="multipart/form-data">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<!-- section one -->
								<div class="col-md-5">
								<h6>Please write your services here.</h6>
									<div class="form-group">
										<label>Title</label>
										<input class="form-control " type="text" value="<?php echo $data['title']; ?>" name="title">
									</div>												
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control"  name="description">
											<?php echo $data['description']; ?>
										</textarea>
									</div>										
									<div class="form-group">
										<label>Integreted topics</label>
										<textarea class="form-control"  name="topics">
											<?php echo $data['topics']; ?>
										</textarea>
										<small>Please topics devited by comma(,)</small>
									</div>
									<div class="form-group">
										<label>Servicess feture image</label>
										<input  class="form-control" type="file" placeholder="Rahim" name="img">
										<img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/<?php echo $data['img']; ?>" alt="No Image">
								</div>										
								</div>
								<!-- Second section -->							
								<div class="col-md-5">
								<h6>Please write your client feedback here.</h6>
									<div class="form-group">
										<label>Feedback</label>
										<textarea class="form-control input-xlg" type="text"  name="clinte_feedback">
											<?php echo $data['clinte_feedback']; ?>
										</textarea>
									</div>
									<div class="form-group">
										<label>Client image</label>
										<input class="form-control input-xlg" type="file"  name="client_image">
										<img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/<?php echo $data['client_image']; ?>" alt="No Image">
									</div>																
								</div>								
							</div>

							<div class="form-group">
								<input  type="hidden" value="<?php echo $data['id']; ?>" name="id">
								<input class="marg-top" type="submit" value="Update" name="services">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>