  <?php
  if (!isset($_SESSION)) {	session_start(); }
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">SERVICES - ADD</span> || <a href="index.php">MY SERVICES</a></h4>
			</div>
		</div>
	</div>
<!-- Add Services terms -->
	<div class="row ">
			<form action="store.php" method="POST"  enctype="multipart/form-data">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
                                    <?php
                                    if(isset($_SESSION['sermsg'])){
                                        echo "<h5 class='text-success'>".$_SESSION['sermsg']."</h5>";
                                        unset($_SESSION['sermsg']);
                                    } ?>
								<!-- section one -->
								<div class="col-md-5">
								<h6>Please write your services here.</h6>
									<div class="form-group">
										<label>Title</label>
										<input class="form-control " type="text" placeholder=" Design Services" name="title">
									</div>	
									<div class="form-group">
										<label>Servicess feture image</label>
										<input  class="form-control" type="file" placeholder="Rahim" name="img">
									</div>												
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control"  placeholder="" name="description"></textarea>
									</div>										
									<div class="form-group">
										<label>Integreted topics</label>
										<textarea class="form-control"  placeholder="web design,Logodesign" name="topics"></textarea>
										<small>Please topics devited by comma(,)</small>
									</div>									
								</div>
								<!-- Second section -->							
								<div class="col-md-5">
								<h6>Please write your client feedback here.</h6>
									<div class="form-group">
										<label>Client image</label>
										<input class="form-control input-xlg" type="file" name="client_image">
									</div>					
									<div class="form-group">
										<label>Feedback</label>
										<textarea class="form-control input-xlg" type="text" placeholder="client feedback" name="clinte_feedback"></textarea>
									</div>																
								</div>								
							</div>

							<div class="form-group">
								<?php
                                    if(isset($_SESSION['serfail'])){
                                        echo "<h5 class='text-danger'>".$_SESSION['serfail']."</h5>";
                                        unset($_SESSION['serfail']);
                                    } ?>
								<input class="marg-top" type="submit" value="Save" name="services">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>