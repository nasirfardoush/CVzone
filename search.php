<?php
//Use class file here
include_once ('vendor/autoload.php');
use App\frontview\Search;
$objSear = new Search;

if (!empty($_GET['keyword'])) {
		$data= $objSear->setData($_GET)->getSearchResult();
}else{	
	header("Location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CVzone || Dashboard</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/admin/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/admin/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/admin/css/bootstrap-datepiker.css" rel="stylesheet" type="text/css">
	<link href="assets/admin/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="assets/admin/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/admin/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/custom.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class=" panel-heading topbar" >
			<div class="logo">
				<a href="index.php"><img src="assets/images/logo.png"></a>
			</div>
			<div class="topInput">


				<form>
					<input class="searchTopInput" type="search" name="keyword">
					<input class="searchSubmit" type="submit" value="SEARCH" name="searchUser">
				</form>


			</div>

			<ul>
				<li>
					<a href="views/admin/users/signup.php">Sign up</a>
				</li>
				<li>
					<a href="views/admin/index.php">Sign in</a>
				</li>
			</ul>
		</div>
		<hr>

		<!--Start  Search result single items -->

		<?php 
			if(!empty($data)){
				foreach ($data as  $info) {   ?>
				 <div class="row searchItem">
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-5">
						<img width="150" height="100" src="assets/images/<?php 
								if (!empty($info['featured_img'])) {
									echo $info['featured_img'];
										}else{ echo "man.jpg"; }?>">
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
						<ul>
			            	<li class="media">
			            		<div class="media-body">
			                		<h6 class="media-heading"><a target="-blank" href="views/frontview/?url=<?php echo $info['username'];?> "><strong><?php echo $info['title']; ?></strong></a></h6>
			                		<ul class="list-inline list-inline-separate text-muted">
			                			<li><a target="-blank" href="views/frontview/?url=<?php echo $info['username'];?>" class="text-success">views/frontview/?url=<?php echo $info['username'];?></a></li>
			                		</ul>

			                		<?php echo substr($info['bio'], 0,500);   
			                			if (strlen($info['bio'])>500) {
			                				echo "...";
			                			}
			                		?>
			            		</div>
			            	</li>
			            </ul>
					</div>
				</div>
				<?php } }else{ ?>

						<h2>Data not mached ! with this ( <strong><?php echo $_GET['keyword']; ?></strong>)</h2>
						<p>--Make sure what are you looking .</p>		
						<p>----Check your splleing.</p>
						<p>--------Try with another word.</p>
						
				<?php } ?>			
		<!-- End Search result single items -->	
			<hr>

				<!-- End Search result single items -->
			<!-- Pgination -->	
<!-- 			<div class="row">	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		            <ul class="pagination pagination-flat pagination-xs no-margin-bottom">
						<li class="disabled"><a href="#">&larr;</a></li>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><span>...</span></li>
						<li><a href="#">58</a></li>
						<li><a href="#">59</a></li>
						<li><a href="#">&rarr;</a></li>
					</ul>
		        </div>
			</div> -->
		</div>
		<!-- /search results -->
			
		</div>
	</div>
</div>


</body>
