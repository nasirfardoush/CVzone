<?php 
namespace App\frontview;
use App\database\Database;
/**
* Class for view user data in front side
*/
class Frontview extends Database{	
	private $username = '';
	private $userId = '';
	private $email = '';
	
	function __construct($data=''){
		parent::__construct();
		if(array_key_exists('url',$data)){
			$this->username = filter_var($data['url'],FILTER_SANITIZE_STRING);

			$this->getUserId();
		}elseif(array_key_exists('username',$data)){
			$this->username = filter_var($data['username'],FILTER_SANITIZE_STRING);

			$this->getUserId();
		}else{
			header('Location:../error.php');
		}


	}

	//Set user id alter by user name
	public function getUserId(){
	$sql  = "SELECT * FROM users WHERE username='$this->username' AND deleted_at='0000-00-00 00:00:00'";
	$stmt = $this->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetch();

	//set user id 
	$this->userId = $data['id'];
	$this->email = $data['email'];
	$this->username = $data['username'];

	}

	//Function for Frontview
	public function index(){
		if (isset($this->userId)) {
			$allData = array();
			$allData['username'] 	= $this->username;
			$allData['email'] 		= $this->email;
			$allData['settings'] 	= $this->getSettingsData();
			$allData['abouts']		= $this->getAboutsData();
			$allData['hobbies']	 	= $this->getHobbiesData();
			$allData['facts']	 	= $this->getFactsData();
			$allData['educations']	= $this->getEducationsData();
			$allData['experiences']	= $this->getExperiencesData();
			$allData['awards']	 	= $this->getAwardsData();				
			$allData['posts']	 	= $this->getPostsData();
			$allData['lastposts']	= $this->getLastPosts();
			$allData['services']	= $this->getServicesData();
			$allData['teaching']	= $this->getTeachingData();
			$allData['skills']	 	= $this->getSkillsData();
			$allData['portfolios']	= $this->getPortfoliosData();

			return $allData;
		}else{
			header('Location:../error.php');
		}
	}

	//Get profiles information
	public function getSettingsData(){
		$sql = "SELECT * FROM settings WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$settings = $stmt->fetchAll();
		return $settings;
	}
	//Get abouts informations	
	public function getAboutsData(){
		$sql = "SELECT * FROM abouts WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$abouts = $stmt->fetchAll();
		return $abouts;
	}		
	//Get hobbies informations	
	public function getHobbiesData(){
		$sql = "SELECT * FROM hobbies WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$hobbies = $stmt->fetchAll();
		return $hobbies;
	}		
	//Get facts informations	
	public function getFactsData(){
		$sql = "SELECT * FROM facts WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$facts = $stmt->fetchAll();
		return $facts;
	}	
	//Get educations informations	
	public function getEducationsData(){
		$sql = "SELECT * FROM educations WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$educations = $stmt->fetchAll();
		return $educations;
	}		
	//Get experiences informations	
	public function getExperiencesData(){
		$sql = "SELECT * FROM experiences WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$experiences = $stmt->fetchAll();
		return $experiences;
	}	
	//Get awards informations	
	public function getAwardsData(){
		$sql = "SELECT * FROM awards WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$awards = $stmt->fetchAll();
		return $awards;
	}		
	//Get posts informations	
	public function getPostsData(){
		$sql = "SELECT * FROM posts WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$posts = $stmt->fetchAll();
		return $posts;
	}		

	//Get last posts informations	
	public function getLastPosts(){
		$postData = $this->getPostsData();
		if(!empty($postData)){
			foreach ($postData as  $value) {};
			$postId = $value['id'];

			$sql = "SELECT * FROM posts WHERE id = $postId ORDER BY id DESC LIMIT 1";
			$stmt = $this->prepare($sql);
			$stmt->execute();
			$lastPost = $stmt->fetch();
		return $lastPost;
		}
	}		
	//Get services informations	
	public function getServicesData(){
		$sql = "SELECT * FROM services WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$services = $stmt->fetchAll();
		return $services;
	}		
	//Get teaching informations	
	public function getTeachingData(){
		$sql = "SELECT * FROM teaching WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$teaching = $stmt->fetchAll();
		return $teaching;
	}	
	//Get skills informations	
	public function getSkillsData(){
		$sql = "SELECT * FROM skills WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$skills = $stmt->fetchAll();
		return $skills;
	}		
	//Get  portfolios informations	
	public function getPortfoliosData(){
		$sql = "SELECT * FROM  portfolios WHERE user_id = $this->userId";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$portfolios = $stmt->fetchAll();
		return $portfolios;
	}
}