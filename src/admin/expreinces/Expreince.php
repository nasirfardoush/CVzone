<?php 
namespace App\admin\expreinces;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Expreince extends Database{
	//properties for gethering about table info
	private $designation 		= '';		
	private $company_name 		= '';		
	private $company_location 	= '';		
	private $start_date 		= '';		
	private $end_date			= '';		
	private $expreince_desc 	= '';		
	private $user_id 			='';	
	private $id 				='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			 $_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			 exit();
		}
	}			

	public function setData($data = ''){
		if(array_key_exists('designation',$data) AND !empty($data['designation'])){
			$this->designation = filter_var($data['designation'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('company_name',$data) AND !empty($data['company_name'])){
			$this->company_name = filter_var($data['company_name'],FILTER_SANITIZE_STRING);

		}	
		if(array_key_exists('company_location',$data) AND !empty($data['company_location'])){
			$this->company_location = filter_var($data['company_location'],FILTER_SANITIZE_STRING);

		}			
		if(array_key_exists('expreince_desc',$data) AND !empty($data['expreince_desc'])){
			$this->expreince_desc = filter_var($data['expreince_desc'],FILTER_SANITIZE_STRING);

		}			
		if(array_key_exists('start_date',$data) AND !empty($data['start_date'])){
			$this->start_date = preg_replace("/[^0-9]/", '', $data['start_date']);
		}			
		if(array_key_exists('end_date',$data) AND !empty($data['end_date'])){
			$this->end_date = preg_replace("/[^0-9]/", '', $data['end_date']);
		}		
		
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}	

	//Insert data into experiences table
	public function store(){
		$date = date('Y-m-d h:i:s');
		$sql    ="INSERT INTO experiences (created_at,unique_id,user_id,designation,company_name,company_location,
									   				start_date,end_date,expreince_desc)
							 VALUES(:created_at,:unique_id,:user_id,:designation,:company_name,:company_location,
													:start_date,:end_date,:expreince_desc) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':created_at'	   =>$date,
				':unique_id'	   =>uniqid(),
				':user_id' 		   =>$this->user_id,
				':designation'	   =>$this->designation,
				':company_name'	   =>$this->company_name,
				':company_location'=>$this->company_location,
				':start_date'      =>$this->start_date,
				':end_date'	       =>$this->end_date,
				':expreince_desc'  =>$this->expreince_desc,
			));
		if ($insert == true) {
			$_SESSION['exmsg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all education info 
	public function index(){
		$sql  = "SELECT * FROM experiences WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single about info 
	public function show(){
		$sql  = "SELECT * FROM experiences WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update about info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		$sql = "UPDATE experiences SET 	updated_at=:updated_at,designation=:designation,company_name=:company_name,
											company_location=:company_location,start_date=:start_date,
													end_date=:end_date,expreince_desc=:expreince_desc
														WHERE id=:id ";
		$stmt = $this->prepare($sql);
		$updated = $stmt->execute(array(
				':updated_at'		=>$date,
				':designation'		=>$this->designation,
				':company_name'		=>$this->company_name,
				':company_location'	=>$this->company_location,
				':start_date'		=>$this->start_date,
				':end_date'			=>$this->end_date,
				':expreince_desc'	=>$this->expreince_desc,
				':id' 				=>$this->id,
		));
		if ($updated == true) {
			$_SESSION['exmsg'] = "Succesfully updated";
			header('Location:index.php');
		}else{
			$_SESSION['exfail'] = "Something worng!";
			header('Location:edit.php');
		}
	}
		//Delete about info 
		public function softDelete(){
			$date = date('Y-m-d h:i:s');
			$sql = "UPDATE experiences SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute();
			if ($updated == true) {
				$_SESSION['exmsg'] = "Succesfully deleted";
				header('Location:index.php');
			}
		}	


}