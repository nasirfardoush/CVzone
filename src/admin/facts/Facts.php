<?php 
namespace App\admin\facts;
use App\database\Database;

/**
* Facts class for access facts Module or facts table
*
*/
class Facts extends Database{
	//properties for gethering facts table info
	private $title = '';					
	private $img = '';	
	private $no_of_items = '';	
	private $user_id ='';	
	private $id ='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			 $_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			 exit();
		}
	}			
	//Set data for facts table
	public function setData($data = ''){
		if(array_key_exists('img',$data)){
			$this->img = $data['img'];
		}		
		if(array_key_exists('title',$data) AND !empty($data['title'])){
			$this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);
		}
		if(array_key_exists('no_of_items',$data) AND !empty($data['no_of_items'])){
			$this->no_of_items = preg_replace("/[^0-9]/", '', $data['no_of_items']);
		}				
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}

	//Insert data into facts table
	public function store(){
		$sql    ="INSERT INTO  facts (unique_id,user_id,img,title,no_of_items) VALUES(:unique_id,:user_id,:img,:title,:no_of_items) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':unique_id'	=>uniqid(),
				':user_id' 		=>$this->user_id,
				':img' 			=>$this->img ,
				':no_of_items' 	=>$this->no_of_items, 				
				':title' 	    =>$this->title 				
			));
			
		if ($insert) {
			$_SESSION['fmsg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all facts info 
	public function index(){
		$sql  = "SELECT * FROM facts WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single hobbies info 
	public function show(){
		$sql  = "SELECT * FROM facts WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update hobbies info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		if (!empty($this->img)) {
			$sql = "UPDATE facts SET 	updated_at=:updated_at, img=:img, title=:title,
													no_of_items=:no_of_items   WHERE id=:id";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute(array(
					':updated_at' 	=>$date,
					':img' 			=>$this->img,
					':title' 		=>$this->title ,
					':no_of_items' 	=>$this->no_of_items ,
					':id' 			=>$this->id,

			));
			if ($updated == true) {
				$_SESSION['fmsg'] = "Succesfully updated";
				header('Location:index.php');
			}else{
				$_SESSION['ffail'] = "Something worng!";
				header('Location:edit.php');
			}
		}else{
			$sql = "UPDATE facts SET 	updated_at=:updated_at, title=:title,	no_of_items=:no_of_items   WHERE id=:id";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute(array(
					':updated_at' 	=>$date,
					':title' 		=>$this->title ,
					':no_of_items' 	=>$this->no_of_items ,
					':id' 			=>$this->id,

			));
			if ($updated == true) {
				$_SESSION['fmsg'] = "Succesfully updated";
				header('Location:index.php');
			}else{
				$_SESSION['ffail'] = "Something worng!";
				header('Location:edit.php');
			}
		}
	}
		//Delete facts info 
		public function softDelete(){
			$date = date('Y-m-d h:i:s');
			$sql = "UPDATE facts SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			$stmt	 = $this->prepare($sql);
			$deleted = $stmt->execute();
			if ($deleted == true) {
				unlink('../../../assets/images/');
				$_SESSION['fmsg'] = "Succesfully deleted";
				header('Location:index.php');
			}

			//For file unlink
			// $sql  = "SELECT * FROM facts WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			// $stmt = $this->prepare($sql);
			// $stmt->execute();
			// $data = $stmt->fetch();
			// $image = "../../../assets/images/".$data['img'];

			// return $image;

		}	


}