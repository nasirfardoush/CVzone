<?php 
namespace App\admin\settings;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Settings extends Database{
	//properties for gethering about table info
	private $title        = '';		
	private $fullname     = '';		
	private $address      = '';		
	private $featured_img = '';	
	private $themecolor   = '';	
	private $user_id      ='';	
	private $id           ='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			 $_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			 exit();
		}
	}			

	public function setData($data = ''){		
		if(array_key_exists('title',$data) AND !empty($data['title'])){
			$this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('fullname',$data) AND !empty($data['fullname'])){
			$this->fullname = filter_var($data['fullname'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('themecolor',$data) AND !empty($data['themecolor'])){
			$this->themecolor = filter_var($data['themecolor'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('address',$data)AND !empty($data['address'])){
			$this->address = filter_var($data['address'],FILTER_SANITIZE_STRING);
		}			
		if(array_key_exists('featured_img',$data)){
			$this->featured_img = filter_var($data['featured_img'],FILTER_SANITIZE_STRING);
		}		
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}

	//View user profile info
	public function show(){
		$sql  = "SELECT * FROM settings WHERE user_id='$this->user_id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;



	}		
	//Update users setting
	public function update(){
		$date = date('Y-m-d h:i:s');
		if (!empty($this->featured_img)) {
			$sql = "UPDATE settings SET updated_at=:updated_at,title=:title,fullname=:fullname,
														address=:address,featured_img=:featured_img,
																themecolor=:themecolor 	 WHERE id=:id";
			$stmt = $this->prepare($sql);
			$updated = $stmt->execute(array(
					':updated_at' 	=>$date,
					':title' 		=>$this->title ,
					':fullname'  	=>$this->fullname ,
					':address' 		=>$this->address,
					':featured_img' =>$this->featured_img,
					':themecolor'   =>$this->themecolor,
					':id' 			=>$this->id,

			));
			if ($updated == true) {
				$_SESSION['smsg'] = "Settings updated";
				header('Location:edit.php');
			}else{
				$_SESSION['sfail'] = "Something worng!";
				header('Location:edit.php');
			}
		}else{
			$sql = "UPDATE settings SET updated_at=:updated_at,title=:title,
												fullname=:fullname,	address=:address,
														themecolor=:themecolor  WHERE id=:id";
					$stmt = $this->prepare($sql);
					$updated = $stmt->execute(array(
							':updated_at' 	=>$date,
							':title' 		=>$this->title ,
							':fullname'  	=>$this->fullname ,
							':address' 		=>$this->address,
							':themecolor' 	=>$this->themecolor,
							':id' 			=>$this->id,

					));
					if ($updated == true) {
						$_SESSION['smsg'] = "Settings updated";
						header('Location:edit.php');
					}else{
						$_SESSION['sfail'] = "Something worng!";
						header('Location:edit.php');
					}
			}			
	}


}