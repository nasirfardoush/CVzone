<?php
namespace App\admin\posts;
use App\database\Database;

/**
 * About class for access post Module or abouts table
 *
 */
class Posts extends Database{
    //properties for gethering post table info
    private $title       = '';
    private $img         = '';
    private $author_name = '';
    private $categories  = '';
    private $description = '';
    private $country_name='';
    private $city_name   ='';
    private $tags        ='';
    private $user_id     ='';
    private $id          ='';


    public function __construct(){

        parent::__construct();
        //User id confirmation
        if (isset($_SESSION['user']['id'])) {
            $this->user_id = $_SESSION['user']['id'];
        }else{
             $_SESSION['loginfail'] = "Access denide!";
             header('Location:../index.php');
             exit();
        }
    }

    public function setData($data = ''){

        if(array_key_exists('title',$data) AND !empty($data['title'])){
            $this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);
        }        
        if(array_key_exists('img',$data)){
            $this->img = $data['img'];
        }
        if(array_key_exists('author_name',$data) AND !empty($data['author_name'])){
            $this->author_name = filter_var($data['author_name'],FILTER_SANITIZE_STRING);

        }
        if(array_key_exists('city_name',$data) AND !empty($data['city_name'])){
            $this->city_name = filter_var($data['city_name'],FILTER_SANITIZE_STRING);

        }
        if(array_key_exists('country_name',$data)){
            $this->country_name = filter_var($data['country_name'],FILTER_SANITIZE_STRING);
        }
        if(array_key_exists('description',$data)){
            $this->description = filter_var($data['description'],FILTER_SANITIZE_STRING);
        }
        if(array_key_exists('tags',$data)){
            $this->tags = filter_var($data['tags'],FILTER_SANITIZE_STRING);
        }
        if(array_key_exists('categories',$data)){
            $this->categories = filter_var($data['categories'],FILTER_SANITIZE_STRING);
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }
   
    //Insert data into posts table
    public function store(){
        $date = date('Y-m-d h:i:s');
        $sql    ="INSERT INTO  posts (created_at,unique_id,title,img,author_name,
                                         categories,description,country_name,
                                                city_name,tags,user_id) 
                                 VALUES(:created_at,:unique_id,:title,:img,:author_name,
                                         :categories,:description,:country_name,
                                                     :city_name,:tags,:user_id);";

        $stmt = $this->prepare($sql);                                           
        $insert = $stmt->execute(array(
                ':created_at'       =>$date,
                'unique_id'         =>uniqid(),
                ':title'            =>$this->title,
                ':img'              =>$this->img,
                ':author_name'      =>$this->author_name ,
                ':categories'       =>$this->categories ,
                ':description'      =>$this->description ,
                ':country_name'     =>$this->country_name ,
                ':city_name'        =>$this->city_name , 
                 ':tags'            =>$this->tags ,               
                ':user_id'          =>$this->user_id,
                    
                ) );
        if ($insert) {
            $_SESSION['pmsg'] = "Succesfully added ";
            header('Location:create.php');
        }

    }
    //View all posts info
    public function index(){
        $sql  = "SELECT * FROM posts WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
        $stmt = $this->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    //View single posts info
    public function show(){
        $sql  = "SELECT * FROM posts WHERE user_id='$this->user_id'  AND unique_id='$this->id'";
        $stmt = $this->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }   

    //View single posts info
    public function lastPost(){
        $sql  = "SELECT * FROM posts WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00' ORDER BY id DESC LIMIT 1";
        $stmt = $this->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    //Update posts info
    public function update(){
    $date = date('Y-m-d h:i:s');
      if (!empty($this->img)) {
           $sql = "UPDATE posts SET 
                            updated_at =:updated_at,
                               title=:title,
                                 img=:img,
                                  author_name=:author_name,
                                    city_name=:city_name,
                                      country_name=:country_name,
                                       description=:description,
                                          tags=:tags,
                                            categories=:categories WHERE id=:id";


            $stmt    = $this->prepare($sql);
            $updated = $stmt->execute(array(
               'updated_at'         =>$date,
                ':title'            =>$this->title,
                ':img'              =>$this->img,
                ':author_name'      =>$this->author_name ,
                ':city_name'        =>$this->city_name ,
                ':country_name'     =>$this->country_name ,
                ':description'      =>$this->description ,
                ':tags'             =>$this->tags ,
                ':categories'       =>$this->categories ,
                ':id'               =>$this->id,

            ));
            if ($updated == true) {
                $_SESSION['pmsg'] = "Succesfully updated";
                header('Location:index.php');
            }else{
                $_SESSION['pfail'] = "Something worng!";
                header('Location:edit.php');
        }
    }else{
          $sql = "UPDATE posts SET 
                            updated_at =:updated_at,
                               title=:title,
                                  author_name=:author_name,
                                    city_name=:city_name,
                                      country_name=:country_name,
                                       description=:description,
                                          tags=:tags,
                                            categories=:categories WHERE id=:id";


            $stmt    = $this->prepare($sql);
            $updated = $stmt->execute(array(
               'updated_at'       =>$date,
                ':title'            =>$this->title,
                ':author_name'      =>$this->author_name ,
                ':city_name'        =>$this->city_name ,
                ':country_name'     =>$this->country_name ,
                ':description'      =>$this->description ,
                ':tags'             =>$this->tags ,
                ':categories'       =>$this->categories ,
                ':id'               =>$this->id,

            ));
            if ($updated == true) {
                $_SESSION['pmsg'] = "Succesfully updated";
                header('Location:index.php');
            }else{
                $_SESSION['pfail'] = "Something worng!";
                header('Location:edit.php');
            }
         }
    }
    //Delete posts info
    public function softDelete(){
        $date = date('Y-m-d h:i:s');
        $sql = "UPDATE posts SET deleted_at='$date' WHERE user_id=$this->user_id AND unique_id='$this->id'";
        $stmt  = $this->prepare($sql);
        $updated = $stmt->execute();
        if ($updated == true) {
            $_SESSION['pmsg'] = "Succesfully deleted";
            header('Location:index.php');
        }
    }



}