<?php 
namespace App\admin\contacts;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Contact extends Database{
	//properties for gethering about table info
	private $user_id ='';	
	private $id ='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			 $_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			 exit();
		}
	}			

	public function setData($data = ''){

		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}


	//View all contacts  info 
	public function index(){
		$sql  = "SELECT * FROM contacts WHERE user_id=$this->user_id AND deleted_at='0000-00-00 00:00:00' ORDER BY id DESC";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}		
	//View all contacts  info 
	public function indexLimit(){
		$sql  = "SELECT * FROM contacts WHERE user_id=$this->user_id AND deleted_at='0000-00-00 00:00:00' ORDER BY id DESC LIMIT 5 ";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}		
	//View all contacts  info 
	public function getUnseenNotification(){
		$sql  = "SELECT  * FROM contacts WHERE 
							user_id=$this->user_id 
								AND status=1 
										AND deleted_at='0000-00-00 00:00:00'  ";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return count($data);
	}	
	//View single contacts info 
	public function show(){
		$sql  = "SELECT * FROM contacts WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
        public function statusUpdate(){
            $sql = "UPDATE contacts SET status='0' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
            $stmt    = $this->prepare($sql);
            $updated = $stmt->execute();
        } 

	//Delete contacts info 
        public function softDelete(){
            $date = date('Y-m-d h:i:s');
            $sql = "UPDATE contacts SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
            $stmt    = $this->prepare($sql);
            $updated = $stmt->execute();
            if ($updated == true) {
                $_SESSION['msg'] = "Succesfully deleted";
                header('Location:index.php');
            }
        }   


}